#Трекер дел
Основная идея нашей программы включает в себя возможности приложений для ведения списков дел (todo list), календарей (online calendar) и трекеров задач (issue tracking, bug tracking) с рядом изменений, расширений и уточнений.

***
##Аналоги
###Списки дел
####*1. Wunderlist*

Преимущества:

  + Возможность использования на различных устройствах и платформах
  + Группировка задач по спискам, а списков - по папкам
  + Наличие горячих клавиш
  + Возможность гибкой настройки функционала
  + Разделение задач на подзадачи
  + Уведомления по электронной почте, на рабочий стол, push-уведомления
  + Заметки и к задачам
  + Поиск по задачам
  + Напоминания в указанное время
  + Возможность прикрепления к задаче голосовых сообщений, картинок и т. п.
  + Добавление задач с помощью электронной почты
  + Наличие общих списков задач для нескольких пользователей
  + Широкие возможности бесплатной версии
  + Возможность резервного копирования
 
 Недостатки:
 
  - Отсутствие геймификации - вовлечения и удержания пользователей
  - Отсутствие разделения задач по дням недели и по месяцам
  - Отсутствие приоритетов задач
  - Нет возможности создания фильтров
  
####*2. Todoist*

Преимущества:

  + Простой и понятный интерфейс сочетающийся с достаточной функциональностью
  + Возможность использования на различных устройствах и платформах
  + Поддержка горячих клавиш
  + Приоритеты задач
  + Разделение задач на подзадачи
  + Поиск по задачам
  + Возможность резервного копирования
  + Синхронизация календаря
  + Комментарии к задачам
  + Возможность давать доступ к задачам другим пользователям
  + Визуализация производительности посредством Todoist-кармы
  + Возможность отмены завершения задач
  + Уведомления по электронной почте, push-уведомления
  + Установка дат выполнения и повторений при написании текста задачи
  + Наличие меток и фильтров
  + Возможность миграции с Wunderlist
  
Недостатки:
  
  - Многие функции доступны лишь в платной версии
  - Надоедливые предложения воспользоваться премиум функциями
  - Отсутствие разделения задач по дням недели и по месяцам
  - И платный и бесплатный аккаунты имеют ограничения (например, по количеству задач)
  
####*3. Any.do*

Преимущества:

  + Возможность использования на различных устройствах и платформах
  + Очень минималистичный и приятный дизайн
  + Прикрепление к задачам фото, видео, аудио и т. п.
  + Возможность создания общей задачи для нескольких пользователей
  + Комментарии к задачам
  + Быстрое управление задачами с помощью Any.do Moment
  + Сортировка задач по датам исполнения и папкам
  + Разделение задач на подзадачи
  + Возможность привязывать напоминания к местоположению
  + Синхронизация задач на различных устройствах и синхронизация с Google Task
  + Возможность отправки уведомлений о созданной задаче другим пользователям
  
Недостатки:

  - Отсутствие развитой системы приоритетов задач
  - Отсутствие геймификации - вовлечения и удержания пользователей
  - Минималистичный дизайн может затруднять освоение
  - Многие функции доступны лишь в премиум версии
  
###Календари

####*1. Google calendar*

Преимущества:

  + Возможность использования на различных устройствах и платформах
  + Простота использования
  + Поддержка множества языков
  + Возможность создания нескольких календарей и их импортирования от других людей
  + Наличие горячих клавиш
  + Возможность совместного использования календаря
  + Напоминание о событиях по электронной почте и с помощью Push-уведомлений
  + Возможность добавлять развернутые описания мероприятий
  + Синхронизация со множеством приложений и программ
  + Возможность гибкой настройки функционала
  + Полностью бесплатен
  + Система цветов для удобного ориентирования между событиями
  + Импорт календарей в формате iCalendar и  Microsoft Outlook
  
 Недостатки:
 
  -  Кроме событий имеются задачи, но с довольно ограниченным функционалом
  
####*2. Яндекс календарь*

Преимущества:

  + Возможность использования на различных устройствах и платформах
  + Поддержка множества языков
  + Полностью бесплатен
  + Простота использования
  + Интеграция с другими сервисами компании Яндекс, такими как Яндекс.Погода, Яндекс.Афиша, Яндекс.Почта и т. д.
  + Возможность совместного использования календаря
  + Напоминание о событиях по электронной почте, с помощью Push-уведомлений и SMS-сообщений
  + Система цветов для удобного ориентирования между событиями из разных календарей
  + Возможность создания дел и списков дел с напоминаниями, приоритетами и привязкой к календарю
  + Импорт календарей в формате iCalendar
  
 Недостатки:
 
  -  Невозможно выделять подряд несколько дней, часов и т. д. (доступно только прописыванием вручную)
  - Довольно ограниченные возможности настройки
  
###Трекеры задач

####*1. Яндекс трекер*

Преимущества:

  + Кроссплатформенность
  + Подсказки, помогающие изучить все возможности трекера
  + Обратная связь с разработчиками
  + Сортировка задач по фильтрам
  + Разделение работы с задачами на 3 типа: как автор, как исполнитель и как наблюдатель
  + Поиск по задачам
  + Возможность создания шаблонов для задач и комментариев
  + Наличие дашбордов, позволяющих отслеживать состояние задач на одной странице посредством различных статистических данных
  + Возможность организовать работу в соответствии с [методологией Agile](https://en.wikipedia.org/wiki/Agile_software_development)
  + Гибкая настройка всей системы
  + Возможность добавления в избранное и подписки на важные задачи
  + Обновление задач в реальном времени
  + Позволяет распределять задачи, расставлять приоритеты, связывать вместе дела
  
 Недостатки:
 
  - Бесплатно предоставляется лишь урезанная демо-версия
  - Сложен в освоении
  
####*2. Redmine*

Преимущества:

  + Кроссплатформенность
  + Поддержка нескольких проектов
  + Полностью бесплатен
  + Поддержка популярных СУБД, таких как MySQL, PostgreSQL, Oracle и др.
  + Использование [диаграмм Гантта](https://en.wikipedia.org/wiki/Gantt_chart)
  + Основанная на ролях система доступа
  + Множество плагинов, превращающих систему в более мощный инструмент управления
  + Ведение форумов, новостей, документов для каждого проекта
  + Поддержка множества языков
  + Позволяет отслеживать время потраченное на проект каждым из пользователей, что в дальнейшем может быть использовано для оценки вклада каждого участника в проект и стоимости разработки
  + Возможность самостоятельной регистрации новых пользователей
  + Получение уведомлений посредством RSS-потоков и электронной почты
  + Возможность отслеживания ошибок
  
 Недостатки:
 
  - Нельзя давать права пользователю на определённую версию проекта или отдельную задачу. Приходится давать доступ ко всему проекту
  - Интеграция с git возможна только если Redmine и git-репозиторий находятся на одном сервере
  - Нельзя скрывать переписку программистов от клиентов
  - Управлять правами доступа к отдельным файлам или документам также не представляется возможным

***  
##Сценарии использования

  + Пошаговые списки задач (доступ к следующей задаче появляется при выполнении текущей). Например, написание лабораторной работы - прежде чем сделать основную версию, нужно сделать прототип, перед этим предварительно продумав основные детали проекта
  + Использование шаблонных задач состоящих из подзадач в качестве чек-листа. Например, собираясь в путешествие, хотелось бы не забыть взять с собой базовые вещи которые всегда нужны в путешествиях
  + Задача уборки блока в общежитии. Подзадачи распределяются между жильцами блока. Постоянные уведомления, с определённой периодичностью, для невыполнивших свою подзадачу. Приоритет задачи в зависимости от степени важности уборки (возрастания шанса проверки блока). Задача автоматически появляется у всех жителей блока.
  + Задание дел актуальных только в определённые промежутки времени (возможно, только в один временной отрезок) с помощью календаря. 
  + Создание бессрочных задач, таких как прочтение книги или просмотр определённого фильма. Напоминания о них приходят в период свободный от обычных задач.
  + Использование в разработке проекта с разделением задач каждого участника, распределением прав доступа к задачам, наличием 3 типов пользователей: авторы - имеют полные возможности взаимодействия с проектом, исполнители - имеют доступ к исполнению лишь своих задач, наблюдатели - могут только следить за выполнением задач. Оценка вклада каждого участника, с помощью статистики количества выполненных дел и времени потраченного на них.
  
***  
##Планируемые возможности

  + Наличие проектов, которые выводят возможности общих задач на новый уровень
  + Наличие самых используемых горячих клавиш
  + Группировка задач по спискам, а списков - по папкам
  + Разделение задач на подзадачи
  + Бессрочные задачи, напоминания о которых приходят в свободное от обычных задач время
  + Уведомления по электронной почте
  + Поиск по задачам
  + Напоминания в указанное время
  + Простой и понятный интерфейс сочетающийся с достаточной функциональностью
  + Приоритеты задач
  + Комментарии к задачам
  + Возможность раздачи прав на доступ к отдельным задачам и документам связанным с ними
  + Шаблонные задачи используемые в качестве чек-листов
  + Наличие тегов и фильтров
  + Возможность создания общей задачи для нескольких пользователей
  + Графики визуализирующие прогресс выполнения задач
  + Пошаговые списки задач
  + Перемещение задач в папку "отложенные"
  + Отслеживание времени потраченного на задачу каждым пользователем
  + Простота освоения
  + Разделение работы с задачами на 3 типа: как автор, как исполнитель и как наблюдатель
  + Разделения задач по дням недели и по месяцам
  + При входе в систему, первое что появляется перед пользователем  - список дел на сегодня
  + Одна из форм статистики - отображение задач по группам "Завершённые", "В процессе выполнения", "Отложенные" и т. д.
  + Визуализация дерева подзадач сложных проектов
  
***
##Логическая архитектура

Основные сущности:

  + __*Подзадача.*__ Наименьшая единица иерархии использования задач. Обязательно является частью сущности "Задача". Подзадача может быть только бессрочной.
  + __*Задача.*__ Одна из главных сущностей приложения. Может как включать подзадачи, так и не включать их. Возможно завершить задачу без выполнения подзадач, но при этом будет выводиться уточняющее уведомление. Может быть бессрочной, со сроком выполнения и шаблонной. Имеет возможность задания времени выполнения вручную и через календать. При истечении срока выполнения пропадает из списка и попадает в статистику как "Проваленная". Может быть отложена, при этом время выполнения удаляется. Поддерживает теги, по которым возможен поиск, в тексте задачи. Содержит в себе такое поле, как комментарии. Может иметь один из 4 приоритетов или не иметь вовсе.
  + __*Список.*__ Включает в себя одну или несколько задач. Может быть добавлен в папку и в проект. При уничтожении удаляет все содержащиеся в нем задачи.
  + __*Календарь.*__ Используется для задания сроков исполнения задач и их визуализации. 
  + __*Проект.*__ Одна из главных сущностей приложения. Самый верхний компонент в иерархии задач. Поддерживает 3 статуса пользователей: наблюдатели, исполнители и авторы. У авторов есть доступ ко всем задачам проекта, у наблюдателей и исполнителей - только к разрешенным автором задачам. После выполнения задачи в проекте, она из него не удаляется. Содержит наиболее развернуютую статистику выполнения задач и времени, потраченного на их выполнение каждым из участников.
  + __*Папка.*__ Включает себя один, несколько или неодного списка. Может быть добавлена в проект. При уничтожении удаляет все содержащиеся в ней списки.
  + __*Пользователь.*__ Данная сущность включает в себя все папки, списки и задачи конкретного пользователя. Содержит информацию об учатии в проектах, а именно статусы "автор", "исполнитель" и "наблюдатель". При последних двух статусах предоставляется доступ лишь к определенным задачам (возможно всем).
  + __*Уведомления.*__ Поддерживаются 2 типа уведомлений: Push-уведомления и по электронной почте. Для бессрочных задач появляются в свободное время
  + __*База данных.*__ Хранит информацию о различных пользователях - все их задачи, проекты, статистику, списки, папки, настройки и т. д.
  + __*Статистика.*__ Сущность, отвечающая за сбор статистических данных. Используется при выведении информации о всех видах задач, отслеживании прогресса проекта.

Интерфейс окончательной версии (продуманы лишь основные части):

Начальной страницей сайта является окно авторизации. После входа в аккаунт перед пользователем появляется список задач на сегодня. Если таких задач нет, то отображается список бессрочных задач. 

В левой нижней части экрана находятся папки и списки, ниже - кнопки их создания. В левой верхней - автоматичекие списки задач "Сегодня", "Завтра" и "Бессрочные задачи". Слева в шапке находятся две вкладки - "Повседневные задачи" и "Проекты". Справа в шапке иконка пользователя. При нажатии на неё появляется выпадающий список с пунктами "Настройка", "Горячие клавиши", "Статистика" и "Календарь".

В пункте "Настройка": включение и отключение уведомлений по почте и Push-уведомлений.

В пункте "Горячие клавиши": описание доступных быстрых клавиш.

В пункте "Статистика": 4 столбца с задачами и проектами - "Завершенные", "В процессе выполнения", "Отложенные" и "Проваленные".

В пункте "Календарь": визуализация распределения времени на задачи.

Остальное пространство во вкладке "Повседневные задачи" занимают, как ни странно, сами задачи, упорядоченные по приоритету. Над ними находится кнопка для добавления новой задачи и поиска по уже существующим. При нажатии на задачу или создании новой, справа появляется меню добавления задачи. Оно содержит поле для ввода названия, выбор приоритета задачи, галочку о бессрочности задачи, поле с кнопкой для добавления подзадачи, выбор списка, включающего задачу, галочку статуса "шаблонная", поле для ввода сроков выполнения и иконка появления календаря с выбором времени выполнения.

Во вкладке "Проекты" похожий на первую вкладку интерфейс. Первоначально перед пользователем появляется список проектов, а не задач на сегодня. Слева вверху добавляется категория "Текущие проекты". Напротив названий проектов отображается кнопка статистики проекта. При клике на проект осуществляется переход к его задачам. Вверху, кроме поиска и кнопки добавления новой задачи, присутствует кнопка добаления новых пользователей. При клике на неё, слева появляется меню с выбором двух статусов - "Наблюдатель" и "Исполнитель". Также в меню присутствует краткий список задач проекта, к которым можно выдавать доступ. При клике на определённую задачу проекта, в меню можно добавлять пользователей имеющих доступ к ней.

***
##Этапы разработки

####*1. Прототип*

Прототип будет представлять собой консольную версию с базовыми возможностями и простейшими взаимодействиями между задачами. Послужит основой структуры приложения Предоставит возможность пользования лишь одному пользователю.

Задача-минимум:

  + Консольный интерфейс
  + Сохранение всех данных в файл
  + Возможность создания задач, подзадач, списков, папок
  + Приоритеты задач
  + Комментарии к задачам
  + 4 основные группы задач - "Отложенные", "Завершёные", "В процессе выполнения", "Проваленные"
  + Задание дат и времени активности задач вручную
  + Бессрочные задачи
  + Теги задач
  
Задача-максимум:

  + Возможность создания фильтров и сортировки по ним
  + Пошаговые задачи
  + Разделения задач по дням недели и по месяцам

####*2. Основная версия*

Основная версия будет реализовывать весь бэкэнд, за исключением отдельных деталей. Позволит добавлять несколько пользователей. Главной задачей является создание такого мощного инструмента, как проекты.

Задача-минимум:

  + Возможность создания нескольких пользователей
  + Сохранение информации в базе данных
  + Возможность создания проектов
  + Возможность разбиения задач между пользователями
  + Отслеживание времени потраченного на задачу каждым пользователем
  + Распределение прав доступа
  + Поиск по задачам

Задача-максимум:

  + Сбор данных для дерева подзадач проектов
  + Шаблонные задачи

####*3. Веб-версия*

Веб-версия будет представлять собой весь проект, включающий в себя основную версию представленную с помощью веб-сайта. Основной задачей данного этапа является разработка дизайна и реализация регистрации.

Задача-минимум:

  + Разработать приятный и понятный дизайн сайта соответствующий всем возможностям основной версии
  + Push-уведомления
  + Система регистрации пользователей
  + Разделение работы с задачами на 3 типа: как автор, как исполнитель и как наблюдатель
  + Наличие самых используемых горячих клавиш

Задача-максимум:

  + Задание дат и времени активности задач с помощью выделения области мышью
  + Визуализация дерева подзадач проектов
  + Уведомления по электронной почте
  + Графики визуализирующие прогресс выполнения задач

***
##Сроки

Прототип - до 10 апреля

Основная версия - до 5 мая

Веб-версия - до 31 мая

***
## Статус

Программный продукт в стадии проектирования.

Переосмыслена структура проекта, решено:

  + Создать базовый абстрактный класс для моделей
  + Разделить сущности по отдельным файлам
  + Все модели переместить в папку с соответствующим названием
  + Избавиться от класса User, в пользу использования лишь никнейма
  + Избавиться от класса Project, усложняющего логику и не приносящего особой функциональности
  + Вынести получение сессиии и базового типа в отдельный файл
  + Создать сущность "исполнитель" для предоставления доступа к задаче не только автору

Создан отдельный класс шаблонной задачи для планировщика, так как в обычной задаче содержатся лишние поля, не используемые планировщиком для создания очередной задачи.