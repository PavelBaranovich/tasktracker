from setuptools import (
    setup,
    find_packages
)

setup(
    name="python_tracker",
    version="0.0.1",
    author="Pavel Baranovich",
    author_email="baranovich.pavel@yandex.ru",
    description="Simple app for task tracking",
    url="https://bitbucket.org/PavelBaranovich/pythonlabs",
    license='',
    packages=find_packages(),
    entry_points={
        "console_scripts": ["tracker = cli.main:main"]
    },
    install_requires=["SQLAlchemy", "python-dateutil"],
    test_suite='lib.tests'
)