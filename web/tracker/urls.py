from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

app_name = "tracker"

task_fields_patterns = [
    url(r'^executor/add/$', views.add_executor, name='executor_add'),
    url(r'^executor/(?P<executor_id>\d+)/delete/$', views.delete_executor, name='executor_delete'),

    url(r'^notification/add/$', views.add_notification, name='notification_add'),
    url(r'^notification/(?P<notification_id>\d+)/delete/$', views.delete_notification, name='notification_delete'),

    url(r'^tag/add/$', views.add_tag, name='tag_add'),
    url(r'^tag/(?P<tag_id>\d+)/delete/$', views.delete_task_tag, name='task_tag_delete'),

    url(r'^subtask/attach/$', views.attach_subtask, name='subtask_attach'),
    url(r'^subtask/(?P<subtask_id>\d+)/detach/$', views.detach_subtask, name='subtask_detach'),
]

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^users/singup/$', views.signup, name='signup'),
    url(r'^users/login/$', auth_views.login, name='login'),
    url(r'^users/logout/$', auth_views.logout, {'template_name': 'registration/logout.html'}, name='logout'),

    url(r'^search/task/$', views.search_task, name='task_search'),

    url(r'^tag/(?P<tag_id>\d+)/show/$', views.show_tag, name='tag_show'),
    url(r'^tag/(?P<tag_id>\d+)/edit/$', views.edit_tag, name='tag_edit'),
    url(r'^tag/(?P<tag_id>\d+)/delete/$', views.delete_tag, name='tag_delete'),

    url(r'^folder/add/$', views.add_folder, name='folder_add'),
    url(r'^folder/(?P<folder_id>[0-9]+)/edit/$', views.edit_folder, name='folder_edit'),
    url(r'^folder/(?P<folder_id>[0-9]+)/delete/$', views.delete_folder, name='folder_delete'),

    url(r'^planner/list/$', views.planner_list, name='planner_list'),
    url(r'^planner/add/$', views.add_planner, name='planner_add'),
    url(r'^planner/(?P<planner_id>[0-9]+)/delete/$', views.delete_planner, name='planner_delete'),
    url(r'^planner/(?P<planner_id>[0-9]+)/edit/$', views.edit_planner, name='planner_edit'),
    url(r'^planner/(?P<planner_id>[0-9]+)/show/$', views.show_planner, name='planner_show'),

    url(r'^folder/(?P<folder>[a-z0-9]+)/task/list/$', views.list_task, name='task_list'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/add/$', views.add_task, name='task_add'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/', include(task_fields_patterns)),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/edit/$', views.edit_task, name='task_edit'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/show/$', views.show_task, name='task_show'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/delete/$', views.delete_task, name='task_delete'),
    url(r'^folder/(?P<folder>[a-z0-9]+)/task/(?P<task_id>\d+)/complete/$', views.complete_task, name='task_complete'),
]