from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from task_tracker_lib.models.task import Status, Priority
from task_tracker_lib.models.planner import IntervalType
from task_tracker_lib.api.exceptions import (
    TrackerLibraryError,
    AttachError,
    DetachError
)
from tracker import get_library_interface
from .handlers import (
    error_handler,
    check_planners,
    check_notifications
)
from .forms import (
    TaskForm, 
    FolderForm, 
    ExecutorForm,
    NotificatonForm,
    TagForm,
    PlannerForm,
    SubtaskForm,
    TaskSearchForm
)


@check_planners
@check_notifications
def home(request):
    return render(request, 'tracker/index.html')


@check_planners
@check_notifications
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('tracker:home')
    else:
        form = UserCreationForm()

    return render(request, 
                  'registration/signup.html', 
                  {'form': form})


@login_required
@check_planners
@check_notifications
def list_task(request, folder):
    library_interface = get_library_interface()
    user = request.user.username

    if folder == "without":
        tasks = library_interface.get_tasks(user,
                                            without_folder=True,
                                            without_parent_task=True,
                                            status=Status.IN_PROGRESS)
        tasks.extend(library_interface.get_tasks(user,
                                                 without_folder=True,
                                                 without_parent_task=True,
                                                 status=Status.TODO))
    elif folder == "archive":
        tasks = library_interface.get_tasks(user, 
                                            status=Status.FAILED,
                                            without_parent_task=True)
        tasks.extend(library_interface.get_tasks(user, 
                                                 status=Status.DONE,
                                                 without_parent_task=True))
    elif folder == "executor":
        tasks = library_interface.get_tasks(user, 
                                            as_executor=True, 
                                            without_parent_task=True)
    elif folder.isdigit() and int(folder) != 0:
        folder_id = int(folder)
        tasks = library_interface.get_tasks(user,
                                            folder_id=folder_id,
                                            without_parent_task=True,
                                            status=Status.IN_PROGRESS)
        tasks.extend(library_interface.get_tasks(user,
                                                 folder_id=folder_id,
                                                 without_parent_task=True,
                                                 status=Status.TODO))

    folders = library_interface.get_folders(user)
    return render(request, 
                  'tracker/tasks/list.html', 
                  {'tasks': tasks,
                   'folders': folders,
                   'folder': folder})


@login_required
@error_handler
@check_planners
@check_notifications
def add_task(request, folder):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = TaskForm(user, None, request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = Priority(int(form.cleaned_data['priority']))
            description = form.cleaned_data['description']
            folder_id = int(form.cleaned_data['folder_id'])

            start_time = form.cleaned_data['start_time']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            end_time = form.cleaned_data['end_time']
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                task_id = library_interface.create_task(
                    user, name, start_time, end_time,
                    priority, description
                )
                if folder_id != 0:
                    library_interface.add_task_to_folder(
                        user, task_id, folder_id)
            except TrackerLibraryError as e:
                form.add_error(None, str(e))
                return render(request, 
                              'tracker/tasks/add.html', 
                              {'form': form})

            folder = "without" if folder_id == 0 else folder_id
            return redirect('tracker:task_show', folder, task_id)
    else:
        form = TaskForm(
            user, None, 
            initial={
                'folder_id': 0 if folder == "without" else folder
            })

    return render(request, 'tracker/tasks/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def edit_task(request, folder, task_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = TaskForm(user, task_id, request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = Priority(int(form.cleaned_data['priority']))
            description = form.cleaned_data['description']
            folder_id = int(form.cleaned_data['folder_id'])

            start_time = form.cleaned_data['start_time']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            end_time = form.cleaned_data['end_time']
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                library_interface.edit_task(
                    user, task_id, name, start_time, 
                    end_time, priority, description
                )
                if folder_id != -1: # if folder has not changed
                    if folder_id == 0:
                        library_interface.delete_task_from_folder(
                            user, task_id)
                        folder = "without"
                    else:
                        library_interface.add_task_to_folder(
                            user, task_id, folder_id)
                        folder = folder_id
            except (AttachError, DetachError) as e:
                form.add_error('folder_id', str(e))
                return render(request, 
                              'tracker/tasks/edit.html', 
                              {'form': form})
            except TrackerLibraryError as e:
                form.add_error(None, str(e))
                return render(request, 
                              'tracker/tasks/edit.html', 
                              {'form': form})

            return redirect('tracker:task_show', folder, task_id)
    else:
        task = library_interface.get_task(user, task_id)
        form = TaskForm(
            user, task_id,
            initial={
                'name': task.name,
                'start_time': task.start_time,
                'end_time': task.end_time,
                'priority': task.priority.value,
                'description': task.description
            }
        )

    return render(request, 'tracker/tasks/edit.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_task(request, folder, task_id):
    if request.method == 'POST':
        library_interface = get_library_interface()
        user = request.user.username

        library_interface.delete_task(user, task_id, with_task_tree=True)
    return redirect('tracker:task_list', folder)


@login_required
@error_handler
@check_planners
@check_notifications
def show_task(request, folder, task_id):
    library_interface = get_library_interface()
    user = request.user.username

    if folder != 'executor':
        task = library_interface.get_task(user, task_id)
    else:
        task = library_interface.get_task(user, task_id, as_executor=True)
    return render(request, 
                  'tracker/tasks/show.html', 
                  {'folder':folder, 'task': task})


@login_required
@error_handler
@check_planners
@check_notifications
def complete_task(request, folder, task_id):
    if request.method == 'POST':
        library_interface = get_library_interface()
        user = request.user.username

        library_interface.complete_task(user, task_id)
    return redirect('tracker:task_list', folder)


@login_required
@error_handler
@check_planners
@check_notifications
def delete_folder(request, folder_id):
    library_interface = get_library_interface()
    user = request.user.username

    library_interface.delete_folder(user, folder_id)

    return redirect('tracker:task_list', folder='without')


@login_required
@check_planners
@check_notifications
def add_folder(request):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = FolderForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                library_interface.add_folder(user, name)
            except ValueError as e:
                form.add_error('name', str(e))
                return render(request, 
                              'tracker/folders/add.html', 
                              {'form': form})

            return redirect('tracker:task_list', folder='without')
    else:
        form = FolderForm()

    return render(request, 'tracker/folders/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def edit_folder(request, folder_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = FolderForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                library_interface.edit_folder(user, folder_id, name=name)
            except ValueError as e:
                form.add_error('name', str(e))
                return render(request, 
                              'tracker/folders/edit.html', 
                              {'form': form})

            return redirect('tracker:task_list', folder='without')
    else:
        folder = library_interface.get_folder(user, folder_id)
        form = FolderForm(initial={'name': folder.name})

    return render(request, 'tracker/folders/edit.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def add_executor(request, folder, task_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = ExecutorForm(request.POST)
        if form.is_valid():
            executor_username = form.cleaned_data['username']

            try:
                User.objects.get(username=executor_username)
                library_interface.add_executor(
                    user, executor_username, task_id, 
                    in_subtasks=True
                )
            except User.DoesNotExist:
                form.add_error('username', "User doesn't exist")
                return render(request, 
                              'tracker/executors/add.html', 
                              {'form': form})
            except TrackerLibraryError as e:
                form.add_error('username', str(e))
                return render(request, 
                              'tracker/executors/add.html', 
                              {'form': form})

            return redirect('tracker:task_show', 
                            folder=folder, 
                            task_id=task_id)
    else:
        form = ExecutorForm()

    return render(request, 'tracker/executors/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_executor(request, folder, task_id, executor_id):
    library_interface = get_library_interface()
    user = request.user.username

    executor = library_interface.get_executor(user, executor_id)
    library_interface.delete_executor(
        user, executor.user, task_id, 
        in_subtasks=True
    )

    return redirect('tracker:task_show', folder=folder, task_id=task_id)


@login_required
@error_handler
@check_planners
@check_notifications
def add_notification(request, folder, task_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = NotificatonForm(request.POST)
        if form.is_valid():
            message = form.cleaned_data['message']
            date = form.cleaned_data['date'].replace(tzinfo=None)

            try:
                library_interface.add_notification(
                    user, task_id, date, message)
            except TrackerLibraryError as e:
                form.add_error('date', str(e))
                return render(request, 
                              'tracker/notifications/add.html', 
                              {'form': form})

            return redirect('tracker:task_show', 
                            folder=folder, 
                            task_id=task_id)
    else:
        form = NotificatonForm()

    return render(request, 'tracker/notifications/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_notification(request, folder, task_id, notification_id):
    library_interface = get_library_interface()
    user = request.user.username

    library_interface.delete_notification(user, notification_id)

    return redirect('tracker:task_show', folder=folder, task_id=task_id)


@login_required
@check_planners
@check_notifications
def add_tag(request, folder, task_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                library_interface.add_tag(
                    user, name, task_id)
            except TrackerLibraryError as e:
                form.add_error('name', str(e))
                return render(request, 
                              'tracker/tags/add.html', 
                              {'form': form})

            return redirect('tracker:task_show', 
                            folder=folder, 
                            task_id=task_id)
    else:
        form = TagForm()

    return render(request, 'tracker/tags/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_task_tag(request, folder, task_id, tag_id):
    library_interface = get_library_interface()
    user = request.user.username

    library_interface.delete_tag(user, tag_id, task_id)

    return redirect('tracker:task_show', folder=folder, task_id=task_id)


@login_required
@check_planners
@check_notifications
def planner_list(request):
    library_interface = get_library_interface()
    user = request.user.username

    planners = library_interface.get_planners(user)

    return render(request, 
                  'tracker/planners/list.html', 
                  {'planners': planners})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_planner(request, planner_id):
    if request.method == 'POST':
        library_interface = get_library_interface()
        user = request.user.username

        library_interface.delete_planner(user, planner_id)
    return redirect('tracker:planner_list')


@login_required
@check_planners
@check_notifications
def add_planner(request):
    library_interface = get_library_interface()
    user = request.user.username
    
    if request.method == 'POST':
        form = PlannerForm(user, False, request.POST)
        if form.is_valid():
            interval_type = IntervalType(form.cleaned_data['interval_type'])
            interval_count = form.cleaned_data['interval_count']
            task_id = form.cleaned_data['task_id']
            total_tasks_count = form.cleaned_data['total_tasks_count']
            
            start_time = form.cleaned_data['start_time']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            end_time = form.cleaned_data['end_time']
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                planner_id = library_interface.create_planner(
                    user, interval_type, interval_count, task_id,
                    start_time, end_time, total_tasks_count
                )
            except TrackerLibraryError as e:
                form.add_error(None, str(e))
                return render(request, 
                              'tracker/planners/add.html', 
                              {'form': form})

            return redirect('tracker:planner_show', planner_id)
    else:
        form = PlannerForm(user, False)

    return render(request, 'tracker/planners/add.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def edit_planner(request, planner_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':

        form = PlannerForm(user, True, request.POST)
        if form.is_valid():
            interval_type = IntervalType(form.cleaned_data['interval_type'])
            interval_count = form.cleaned_data['interval_count']
            total_tasks_count = form.cleaned_data['total_tasks_count']
            
            task_id = form.cleaned_data['task_id']
            if int(task_id) == 0:
                task_id = None
            start_time = form.cleaned_data['start_time']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            end_time = form.cleaned_data['end_time']
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            try:
                library_interface.edit_planner(
                    user, planner_id, interval_type, interval_count,
                    task_id, start_time, end_time, total_tasks_count
                )
            except ValueError as e:
                form.add_error('total_tasks_count', str(e))
                return render(request,
                              'tracker/planners/edit.html',
                              {'form': form})
            except TrackerLibraryError as e:
                form.add_error(None, str(e))
                return render(request,
                              'tracker/planners/edit.html',
                              {'form': form})

            return redirect('tracker:planner_show', planner_id)
    else:
        planner = library_interface.get_planner(user, planner_id)
        form = PlannerForm(
            user, True,
            initial={
                'interval_type': planner.interval_type,
                'interval_count': planner.interval_count,
                'start_time': planner.start_time,
                'end_time': planner.end_time,
                'total_tasks_count': planner.total_tasks_count
            }
        )

    return render(request, 'tracker/planners/edit.html', {'form': form})


def _get_task_folder_list(tasks):
    folders = []
    for task in tasks:
        if task.status == Status.FAILED or task.status == Status.DONE:
            folders.append("archive")
        elif task.folder is not None:
            folders.append(task.folder.id)
        else:
            folders.append("without")

    return list(zip(tasks, folders))


@login_required
@error_handler
@check_planners
@check_notifications
def show_planner(request, planner_id):
    library_interface = get_library_interface()
    user = request.user.username

    planner = library_interface.get_planner(user, planner_id)

    return render(request, 
                  'tracker/planners/show.html', 
                  {'planner': planner,
                   'task_folder_list': _get_task_folder_list(
                        planner.created_tasks)})


@login_required
@error_handler
@check_planners
@check_notifications
def attach_subtask(request, folder, task_id):
    folder_id = 0 if folder == "without" else int(folder)
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = SubtaskForm(user, folder_id, request.POST)
        if form.is_valid():
            subtask_id = form.cleaned_data['subtask_id']

            try:
                library_interface.attach_subtask(
                    user, task_id, subtask_id,
                    with_executors=True
                )
            except TrackerLibraryError as e:
                form.add_error('subtask_id', str(e))
                return render(request,
                              'tracker/tasks/attach.html', 
                              {'form': form})

            return redirect('tracker:task_show', 
                            folder=folder, 
                            task_id=task_id)
    else:
        form = SubtaskForm(user, folder_id)

    return render(request, 'tracker/tasks/attach.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def detach_subtask(request, folder, task_id, subtask_id):
    library_interface = get_library_interface()
    user = request.user.username

    library_interface.detach_subtask(user, subtask_id)

    return redirect('tracker:task_show', folder=folder, task_id=task_id)


@login_required
@error_handler
@check_planners
@check_notifications
def show_tag(request, tag_id):
    library_interface = get_library_interface()
    user = request.user.username

    tag = library_interface.get_tag(user, tag_id)

    return render(request, 
                  'tracker/tags/show.html', 
                  {'tag': tag,
                   'task_folder_list': _get_task_folder_list(tag.tasks)})


@login_required
@error_handler
@check_planners
@check_notifications
def delete_tag(request, tag_id):
    library_interface = get_library_interface()
    user = request.user.username

    library_interface.delete_tag(user, tag_id)

    return redirect('tracker:task_list', folder="without")


@login_required
@error_handler
@check_planners
@check_notifications
def edit_tag(request, tag_id):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']

            try:
                library_interface.edit_tag(user, tag_id, name=name)
            except ValueError as e:
                form.add_error('name', str(e))
                return render(request, 
                              'tracker/tags/edit.html', 
                              {'form': form})

            return redirect('tracker:tag_show', tag_id=tag_id)
    else:
        tag = library_interface.get_tag(user, tag_id)
        form = TagForm(initial={'name': tag.name})

    return render(request, 'tracker/tags/edit.html', {'form': form})


@login_required
@error_handler
@check_planners
@check_notifications
def search_task(request):
    library_interface = get_library_interface()
    user = request.user.username

    if request.method == 'POST':
        form = TaskSearchForm(user, request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            if name == "":
                name = None

            planner_id = form.cleaned_data['planner_id']
            if planner_id == "":
                planner_id = None

            description = form.cleaned_data['description']
            if description == "":
                description = None

            as_executor = form.cleaned_data['as_executor']

            priority = form.cleaned_data['priority']
            priority = None if priority == "" else Priority(int(priority))
            status = form.cleaned_data['status']
            status = None if status == "" else Status(int(status))

            folder_id = form.cleaned_data['folder_id']
            folder_id = None if folder_id == "" else int(folder_id)
            if folder_id == 0:
                without_folder = True
            else:
                without_folder = False

            parent_task_id = form.cleaned_data['parent_task_id']
            parent_task_id = None if parent_task_id == "" else int(parent_task_id)
            if parent_task_id == 0:
                without_parent_task = True
            else:
                without_parent_task = False

            start_time = form.cleaned_data['start_time']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            end_time = form.cleaned_data['end_time']
            if end_time is not None:
                end_time = end_time.replace(tzinfo=None)

            tasks = library_interface.get_tasks(
                user, 
                name=name,
                status=status,
                start_time=start_time,
                end_time=end_time,
                priority=priority,
                description=description,
                parent_task_id=parent_task_id, 
                planner_id=planner_id,
                folder_id=folder_id, 
                without_folder=without_folder,
                without_parent_task=without_parent_task,
                as_executor=as_executor
            )
            if as_executor:
                task_folder_list = list(zip(tasks, ['executor'] * len(tasks)))
            else:
                task_folder_list = _get_task_folder_list(tasks)

            return render(request,
                          'tracker/tasks/search_result.html', 
                          {'task_folder_list': list(task_folder_list)})
    else:
        form = TaskSearchForm(user)

    return render(request, 'tracker/tasks/search.html', {'form': form})