from django.shortcuts import redirect
from django.contrib import messages
from django.http import Http404
from task_tracker_lib.api.exceptions import (
    TrackerLibraryError,
    ObjectNotFoundError
)
from tracker import get_library_interface


def check_notifications(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            library_interface = get_library_interface()
            missed_notifications = (library_interface
                                    .set_missed_notifications_as_shown(
                                        request.user.username))

            for notification in missed_notifications:
                task = notification.task
                message = "Notification for task with id {}: ".format(task.id)
                if notification.message:
                    message += notification.message
                else:
                    message += "Don't forget to complete task '{}'".format(task.name)

                messages.success(request, message)

        return func(request, *args, **kwargs)

    return wrapper


def check_planners(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            get_library_interface().generate_planners_tasks(
                request.user.username)

        return func(request, *args, **kwargs)

    return wrapper


def error_handler(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ObjectNotFoundError as e:
            raise Http404(str(e))
        except TrackerLibraryError:
            redirect('tracker:home')

    return wrapper