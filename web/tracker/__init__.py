from task_tracker_lib.api.interface import Interface
from django.conf import settings


def get_library_interface():
    database_settings = settings.DATABASES["default"]
    dialect = database_settings["DIALECT"]
    name = database_settings["NAME"]
    return Interface("{}:///{}".format(dialect, name))
