from datetime import datetime
from django import forms
from tracker import get_library_interface
from task_tracker_lib.models.task import Priority, Status
from task_tracker_lib.models.planner import IntervalType

DATE_FORMAT = '%Y-%m-%d %H:%M'


class TaskForm(forms.Form):
    required_css_class = 'required'

    name = forms.CharField(max_length=50)
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
        help_text="Input example: '{}'"
                  .format(datetime.now().strftime(DATE_FORMAT)),
        required=False
    )
    priority = forms.ChoiceField(
        choices=[(priority.value, priority.name) for priority in Priority]
    )
    description = forms.CharField(
        widget=forms.Textarea, 
        max_length=300, 
        required=False
    )
    folder_id = forms.ChoiceField()

    def __init__(self, user, task_id, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        library_interface = get_library_interface()
        
        folders = library_interface.get_folders(user)
        folder_choices_list = [(folder.id, folder.name) for folder in folders]
        folder_choices_list.insert(0, (0, "without folder"))
        
        if task_id is not None:
            task = library_interface.get_task(user, task_id)
            folder_choices_list.insert(0, (-1, "not change"))
        else:
            task = None
        self.fields['folder_id'] = forms.ChoiceField(
            choices=folder_choices_list,
            label='Folder'
        )
        self.fields['start_time'].initial = format(datetime.now(), DATE_FORMAT)

        if (task is not None) and (task.parent_task_id is not None):
            self.fields['folder_id'].widget.attrs['disabled'] = 'disabled'


class NotificatonForm(forms.Form):
    required_css_class = 'required'

    message = forms.CharField(max_length=100, required=False)
    date = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT)
    )

    def __init__(self, *args, **kwargs):
        super(NotificatonForm, self).__init__(*args, **kwargs)

        self.fields['date'].initial = format(datetime.now(), DATE_FORMAT)


class ExecutorForm(forms.Form):
    required_css_class = 'required'

    username = forms.CharField(max_length=150)


class PlannerForm(forms.Form):
    required_css_class = 'required'

    interval_type = forms.ChoiceField(
        help_text='Type of interval between tasks creation',
        choices=[(interval.value, interval.name) for interval in IntervalType]
    )
    interval_count = forms.IntegerField(
        help_text='Count of planner interterval type',
        initial=1,
        min_value=1
    )
    task_id = forms.ChoiceField()
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
        help_text="Input example: '{}'"
                  .format(datetime.now().strftime(DATE_FORMAT)),
        required=False
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
        help_text="Input example: '{}'"
                  .format(datetime.now().strftime(DATE_FORMAT)),
        required=False
    )
    total_tasks_count = forms.IntegerField(
        help_text='Total number of tasks required to create',
        min_value=1, 
        required=False
    )

    def __init__(self, user, edit_form, *args, **kwargs):
        super(PlannerForm, self).__init__(*args, **kwargs)

        tasks = get_library_interface().get_tasks(user)
        task_choices_list = (
            [(task.id, "id:{} name:{}".format(task.id, task.name))
                for task in tasks]
        )
        if edit_form:
            task_choices_list.insert(0, (0, 'not change'))

        self.fields['task_id'] = forms.ChoiceField(
            choices=task_choices_list,
            label='Task',
            help_text='Task from which template is created'
        )


class SubtaskForm(forms.Form):
    required_css_class = 'required'

    subtask_id = forms.ChoiceField()

    def __init__(self, user, folder_id, *args, **kwargs):
        super(SubtaskForm, self).__init__(*args, **kwargs)

        if folder_id != 0:
            tasks = get_library_interface().get_tasks(user, 
                                                      without_parent_task=True,
                                                      folder_id=folder_id)
        else:
            tasks = get_library_interface().get_tasks(user, 
                                                      without_parent_task=True,
                                                      without_folder=True)
        subtask_choices_list = (
            [(task.id, "id:{} name:{}".format(task.id, task.name))
                for task in tasks]
        )

        self.fields['subtask_id'] = forms.ChoiceField(
            choices=subtask_choices_list,
            label='Subtask'
        )


class TagForm(forms.Form):
    required_css_class = 'required'

    name = forms.CharField(max_length=20)


class TaskSearchForm(forms.Form):
    name = forms.CharField(
        help_text="Used by search as substring. Not case-sensitive",
        max_length=50, 
        required=False
    )
    start_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
        help_text="Input example: '{}'"
                  .format(datetime.now().strftime(DATE_FORMAT)),
        required=False
    )
    end_time = forms.DateTimeField(
        widget=forms.widgets.DateInput(attrs={'type': 'datetime'}, 
                                       format=DATE_FORMAT),
        help_text="Input example: '{}'"
                  .format(datetime.now().strftime(DATE_FORMAT)),
        required=False
    )
    priority = forms.ChoiceField()
    status = forms.ChoiceField()
    description = forms.CharField(
        help_text="Used by search as substring. Not case-sensitive",
        widget=forms.Textarea, 
        max_length=500, 
        required=False
    )
    folder_id = forms.ChoiceField()
    parent_task_id = forms.ChoiceField()
    planner_id = forms.ChoiceField()
    as_executor = forms.BooleanField(required=False)

    def __init__(self, user, *args, **kwargs):
        super(TaskSearchForm, self).__init__(*args, **kwargs)
        library_interface = get_library_interface()
        
        folders = library_interface.get_folders(user)
        folder_choices_list = [(folder.id, folder.name) for folder in folders]
        folder_choices_list.insert(0, (0, "without folder"))
        folder_choices_list.insert(0, (None, "not select"))
        self.fields['folder_id'] = forms.ChoiceField(
            choices=folder_choices_list,
            label='Folder',
            required=False
        )

        planners = library_interface.get_planners(user)
        planner_choices_list = [(planner.id, planner.id) 
                                for planner in planners]
        planner_choices_list.insert(0, (None, "not select"))
        self.fields['planner_id'] = forms.ChoiceField(
            choices=planner_choices_list,
            label='Created by planner (id)',
            required=False
        )

        tasks = library_interface.get_tasks(user)
        parent_task_choices_list = (
            [(task.id, "id:{} name:{}".format(task.id, task.name))
                for task in tasks]
        )
        folder_choices_list.insert(0, (0, "without parent"))
        parent_task_choices_list.insert(0, (None, "not select"))
        self.fields['parent_task_id'] = forms.ChoiceField(
            choices=parent_task_choices_list,
            label='Parent task',
            required=False
        )

        status_choices_list = [(status.value, status.name) 
                               for status in Status]
        status_choices_list.insert(0, (None, "not select"))
        self.fields['status'] = forms.ChoiceField(
            choices=status_choices_list,
            required=False
        )

        priority_choices_list = [(priority.value, priority.name) 
                                 for priority in Priority]
        priority_choices_list.insert(0, (None, "not select"))
        self.fields['priority'] = forms.ChoiceField(
            choices=priority_choices_list,
            required=False
        )


class FolderForm(forms.Form):
    required_css_class = 'required'

    name = forms.CharField(max_length=20)