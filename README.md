# Python tracker

A simple application that helps to manage your tasks. Allows you to create and edit tasks, planners (to automatically create tasks by template), notifications (for reminding about unfinished tasks), tags and folders (for grouping tasks). You can also give the right to execute tasks to other users.

## Installing:

#### Cloning:
```bash
$ git clone https://PavelBaranovich@bitbucket.org/PavelBaranovich/pythonlabs.git
```
#### Application installing:
```bash
$ cd pythonlabs
$ python3 setup.py install
```
#### Tests running:
```bash
$ python3 setup.py test
```

## Сonfiguring:

#### User changing:
```bash
$ tracker config edit user username "your_name"
```
#### Database settings changing:
```bash
$ tracker config edit database database_url "dialect+driver://username:password@host:port/database"
```

## Library

#### Usage example:
```python
In [1]: from task_tracker_lib.api.interface import Interface

In [2]: database_url = "sqlite:///example.db"

In [3]: interface = Interface(database_url) #connecting to library interface 

In [4]: user = "example_user"

In [5]: task_id = interface.create_task(user=user, name="task")

In [6]: interface.add_tag(user=user, name="tag 1", task_id=task_id)
        #first tag id
Out[6]: 1

In [7]: interface.add_tag(user=user, name="tag 2", task_id=task_id)
        #second tag id
Out[7]: 2

In [8]: task = interface.get_task(user=user, task_id=task_id)

In [9]: print(task)
Task id=1 name=task:
 start time - 15 August 2018  21:08
 end time - no end time
 subtasks ids - [no subtasks]
 description - no description
 status - IN_PROGRESS
 priority - LOW
 folder - no folder
 tags - [tag 1, tag 2]
 executors - [no executors]

In [10]: interface.get_tags(user=user)
         #all user tags
Out[10]: [<Tag(id=1, name=tag 1)>, <Tag(id=2, name=tag 2)>]
```

## Console

#### Working with objects:
```bash
$ tracker task <action> [<args>]
$ tracker planner <action> [<args>]
$ tracker tag <action> [<args>]
$ tracker folder <action> [<args>]
$ tracker notification <action> [<args>]
$ tracker executor <action> [<args>]
$ tracker config <action> [<args>]
```
Use -h or --help for more infomation

#### Usage example:
```bash
$ tracker task add "example name 1"
Task with id 1 added.
$ tracker notification add 1 "15.08.2018 19:00"
Notification with id 1 added.
$ tracker task add "example name 2"
Task with id 2 added.
$ tracker folder add "example name"
Folder with id 1 added.
$ tracker task move 1 -f 1
Task is moved to folder.
$ tracker task move 2 -f 1
Task is moved to folder.
$ tracker folder show 1 -t
Missed notifications:
    Notification id=1:
     message - default
     date - 15 August 2018  19:00
     task id - 1
     is shown - True

Folder id=1 name=example name:
 subfolders_ids - [no subfolders]
 tasks_ids - [1, 2]

    Task id=1 name=example name 1:
     start time - 15 August 2018  18:50
     end time - no end time
     subtasks ids - [no subtasks]
     description - no description
     status - IN_PROGRESS
     priority - LOW
     folder - example name
     tags - [no tags]
     executors - [no executors]

    Task id=2 name=example name 2:
     start time - 15 August 2018  18:52
     end time - no end time
     subtasks ids - [no subtasks]
     description - no description
     status - IN_PROGRESS
     priority - LOW
     folder - example name
     tags - [no tags]
     executors - [no executors]
```