import os
import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta

from task_tracker_lib.models.planner import IntervalType
from task_tracker_lib.models.task import (
    Priority, 
    Status
)
from task_tracker_lib.api.interface import Interface
from task_tracker_lib.api.logger import get_logger
from task_tracker_lib.api.exceptions import (
    ObjectNotFoundError,
    TimeError,
    AttachError,
    DetachError,
    LoopbackError,
    StatusError
)

TASK_NAME = "test_task"
USER_NAME = "test_user"
FOLDER_NAME = "test_folder"
SUBFOLDER_NAME = "test_subfolder"
TAG_NAME = "test_tag"
NEW_OBJECT_NAME = "new_test_name"
EXECUTOR_USER_NAME = "test_executor"
NOTIFICATION_MESSAGE = "test_notification_message"
INVALID_EXECUTOR_USER_NAME = "invalid_test_executor"
PLANNER_INTERVAL_TYPE = IntervalType.MINUTES
PLANNER_INTERVAL_COUNT = 5
INVALID_START_TIME = datetime(2018, 1, 1)
INVALID_END_TIME = datetime(2017, 1, 1)
INVALID_ID = 123
DATABASE_NAME = ":memory:"
DATABASE_URL = "sqlite:///" + DATABASE_NAME


class TestInterface(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logger = get_logger()
        logger.disabled = True

    def setUp(self):
        self.interface = Interface(DATABASE_URL)

    def tearDown(self):
        if DATABASE_NAME != ":memory:":
            os.remove(DATABASE_NAME)

    def test_create_task(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        tasks = self.interface.get_tasks(USER_NAME)
        self.assertEqual(len(tasks), 1)

        task = self.interface.get_task(USER_NAME, task_id)
        self.assertIsNotNone(task.start_time)

        compared_values = (
            (task.user, USER_NAME),
            (task.name, TASK_NAME),
            (task.description, ""),
            (task.folder, None),
            (task.planner, None),
            (task.tree_id, None),
            (task.end_time, None),
            (task.folder_id, None),
            (task.planner_id, None),
            (task.parent_task_id, None),
            (task.priority, Priority.LOW),
            (task.status, Status.IN_PROGRESS),
            (len(task.tags), 0),
            (len(task.subtasks), 0),
            (len(task.executors), 0),
            (len(task.notifications), 0)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)


    def test_create_invalid_task(self):
        self.assertRaises(
            TimeError,
            self.interface.create_task,
            USER_NAME, TASK_NAME, INVALID_START_TIME, INVALID_END_TIME
        )

    def test_get_task(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        task = self.interface.get_task(USER_NAME, task_id)
        self.assertEqual(task.name,TASK_NAME)

    def test_get_invalid_task(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_task,
            USER_NAME,
            task_id=INVALID_ID
        )

    def test_complete_task_as_author(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )

        self.interface.complete_task(USER_NAME, task_id)
        task = self.interface.get_task(USER_NAME, task_id)
        subtask = self.interface.get_task(USER_NAME, subtask_id)
        
        self.assertEqual(task.status, Status.DONE)
        self.assertEqual(subtask.status, Status.DONE)

    def test_complete_task_as_executor(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )
        self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, task_id,
            in_subtasks=True
        )

        self.interface.complete_task(EXECUTOR_USER_NAME, task_id)
        task = self.interface.get_task(
            EXECUTOR_USER_NAME, task_id,
            as_executor=True
        )
        subtask = self.interface.get_task(
            EXECUTOR_USER_NAME, subtask_id,
            as_executor=True
        )
        
        self.assertEqual(task.status, Status.DONE)
        self.assertEqual(subtask.status, Status.DONE)

    def test_complete_invalid_task(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, second_task_id)

        self.assertRaises(
            ObjectNotFoundError,
            self.interface.complete_task,
            EXECUTOR_USER_NAME, first_task_id 
        )

        self.interface.complete_task(USER_NAME, first_task_id)
        self.assertRaises(
            StatusError,
            self.interface.complete_task,
            USER_NAME, first_task_id 
        )

    def test_attach_subtask(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        third_task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        self.interface.attach_subtask(
            USER_NAME, 
            task_id=second_task_id, subtask_id=third_task_id
        )
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=first_task_id, subtask_id=second_task_id
        )

        first_task = self.interface.get_task(USER_NAME, first_task_id)
        second_task = self.interface.get_task(USER_NAME, second_task_id)
        third_task = self.interface.get_task(USER_NAME, third_task_id)

        compared_values = (
            (len(first_task.subtasks), 1),
            (len(second_task.subtasks), 1),
            (first_task.subtasks[0].id, second_task_id),
            (second_task.subtasks[0].id, third_task_id),
            (second_task.tree_id, first_task_id),
            (third_task.tree_id, first_task_id)
        )

        for compared_value in compared_values:
            self.assertEqual(*compared_value)        

    def test_attach_invalid_subtask(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        third_task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_task_to_folder(USER_NAME, first_task_id, folder_id)
        self.assertRaises(
            AttachError,
            self.interface.attach_subtask,
            USER_NAME, 
            task_id=first_task_id, subtask_id=third_task_id
        )

        self.interface.delete_task_from_folder(USER_NAME, first_task_id)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=second_task_id, subtask_id=third_task_id
        )
        self.assertRaises(
            AttachError,
            self.interface.attach_subtask,
            USER_NAME, 
            task_id=first_task_id, subtask_id=third_task_id
        )

        self.interface.attach_subtask(
            USER_NAME, 
            task_id=first_task_id, subtask_id=second_task_id
        )
        self.assertRaises(
            LoopbackError,
            self.interface.attach_subtask,
            USER_NAME, 
            task_id=third_task_id, subtask_id=first_task_id
        )

        end_time = datetime.now() + relativedelta(days=1)
        start_time = end_time + relativedelta(days=1)
        fourth_task_id = self.interface.create_task(
            USER_NAME, TASK_NAME, 
            end_time=end_time
        )
        fifth_task_id = self.interface.create_task(
            USER_NAME, TASK_NAME, 
            start_time=start_time)
        self.assertRaises(
            TimeError,
            self.interface.attach_subtask,
            USER_NAME, 
            task_id=fourth_task_id, subtask_id=fifth_task_id
        )

        end_time = datetime.now()
        start_time = end_time - relativedelta(days=1)
        sixth_task_id = self.interface.create_task(
            USER_NAME, TASK_NAME, start_time, end_time)
        self.interface.edit_task(
            USER_NAME, first_task_id,
            start_time=start_time
        )

        self.assertRaises(
            StatusError,
            self.interface.attach_subtask,
            USER_NAME, 
            task_id=first_task_id, subtask_id=sixth_task_id
        )

    def test_detach_subtask(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        third_task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        self.interface.attach_subtask(
            USER_NAME, 
            task_id=first_task_id, subtask_id=second_task_id
        )
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=second_task_id, subtask_id=third_task_id
        )

        self.interface.detach_subtask(
            USER_NAME, 
            subtask_id=second_task_id
        )

        first_task = self.interface.get_task(USER_NAME, first_task_id)
        second_task = self.interface.get_task(USER_NAME, second_task_id)
        third_task = self.interface.get_task(USER_NAME, third_task_id)
        compared_values = (
            (len(first_task.subtasks), 0),
            (second_task.parent_task_id, None),
            (second_task.tree_id, second_task_id),
            (third_task.tree_id, second_task_id)
        )

        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_detach_invalid_subtask(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.assertRaises(
            DetachError,
            self.interface.detach_subtask,
            USER_NAME, task_id
        )

    def test_delete_task(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        third_task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        self.interface.attach_subtask(
            USER_NAME, 
            task_id=first_task_id, subtask_id=second_task_id
        )
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=second_task_id, subtask_id=third_task_id
        )

        self.interface.add_tag(USER_NAME, TAG_NAME, first_task_id)
        self.interface.add_notification(
            USER_NAME, first_task_id,
            date=datetime.now()
        )
        self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, first_task_id)
        self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, second_task_id)

        self.interface.delete_task(
            USER_NAME, first_task_id, 
            with_task_tree=False
        )

        second_task = self.interface.get_task(USER_NAME, second_task_id)
        third_task = self.interface.get_task(USER_NAME, third_task_id)
        tags = self.interface.get_tags(USER_NAME)
        notifications = self.interface.get_notifications(USER_NAME)

        compared_values = (
            (len(tags), 0),
            (len(notifications), 0),
            (len(second_task.executors[0].tasks), 1),
            (second_task.parent_task_id, None),
            (second_task.tree_id, second_task_id),
            (third_task.tree_id, second_task_id)
        )

        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_edit_task(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        start_time = datetime.now() - relativedelta(days=1)
        end_time = datetime.now() + relativedelta(days=1) 
        priority = Priority.HIGH
        description = "edit_description"
        self.interface.edit_task(
            USER_NAME, task_id, NEW_OBJECT_NAME, start_time, 
            end_time, priority, description
        )

        task = self.interface.get_task(USER_NAME, task_id)
        compared_values = (
            (task.name, NEW_OBJECT_NAME),
            (task.start_time, start_time),
            (task.end_time, end_time),
            (task.priority, priority),
            (task.description, description),
            (task.status, Status.IN_PROGRESS)
        )

        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_invalid_edit_task(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        end_time = datetime.now()
        start_time = end_time + relativedelta(days=1)
        self.assertRaises(
            TimeError,
            self.interface.edit_task,
            USER_NAME, task_id,
            start_time=start_time, end_time=end_time
        )

    def test_add_task_to_folder(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)

        self.interface.add_task_to_folder(USER_NAME, task_id, folder_id)

        folder = self.interface.get_folder(USER_NAME, folder_id)
        task = self.interface.get_task(USER_NAME, task_id)
        subtask = self.interface.get_task(USER_NAME, subtask_id)
        self.assertIn(subtask, folder.tasks)
        self.assertIn(task, folder.tasks)
        self.assertEqual(len(folder.tasks), 2)

    def test_edit_folder(self):
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)

        self.interface.edit_folder(USER_NAME, folder_id, NEW_OBJECT_NAME)

        folder = self.interface.get_folder(USER_NAME, folder_id)
        self.assertEqual(folder.name, NEW_OBJECT_NAME)

    def test_invalid_edit_folder(self):
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_folder(USER_NAME, NEW_OBJECT_NAME)

        self.assertRaises(
            ValueError,
            self.interface.edit_folder,
            USER_NAME, folder_id, NEW_OBJECT_NAME
        )

    def test_add_invalid_task_to_folder(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)

        self.assertRaises(
            AttachError,
            self.interface.add_task_to_folder,
            USER_NAME, subtask_id, folder_id
        )

        self.interface.add_task_to_folder(USER_NAME, task_id, folder_id)
        self.assertRaises(
            AttachError,
            self.interface.add_task_to_folder,
            USER_NAME, task_id, folder_id
        )

    def test_delete_task_from_folder(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_task_to_folder(USER_NAME, task_id, folder_id)

        self.interface.delete_task_from_folder(USER_NAME, task_id)
        
        task = self.interface.get_task(USER_NAME, task_id)
        subtask = self.interface.get_task(USER_NAME, subtask_id)
        self.assertIsNone(task.folder)
        self.assertIsNone(subtask.folder)

    def test_delete_invalid_task_from_folder(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)

        self.assertRaises(
            DetachError,
            self.interface.delete_task_from_folder,
            USER_NAME, task_id
        )

        subtask_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.attach_subtask(
            USER_NAME, 
            task_id=task_id, subtask_id=subtask_id
        )
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_task_to_folder(USER_NAME, task_id, folder_id)

        self.assertRaises(
            DetachError,
            self.interface.delete_task_from_folder,
            USER_NAME, subtask_id
        )     

    def test_get_folder(self):
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        folder = self.interface.get_folder(USER_NAME, folder_id)
        self.assertEqual(folder.name, FOLDER_NAME)

    def test_get_invalid_folder(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_folder,
            USER_NAME,
            folder_id=INVALID_ID
        )

    def test_add_folder(self):
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_folder(
            USER_NAME, SUBFOLDER_NAME, 
            parent_folder_id=folder_id
        )

        folders = self.interface.get_folders(
            USER_NAME, 
            parent_folder_id=folder_id
        )
        self.assertEqual(len(folders), 1)

        folders = self.interface.get_folders(USER_NAME)
        self.assertEqual(len(folders), 2)

        folder = self.interface.get_folder(USER_NAME, folder_id)
        compared_values = (
            (folder.user, USER_NAME),
            (folder.name, FOLDER_NAME),
            (len(folder.tasks), 0),
            (len(folder.subfolders), 1),
            (folder.parent_folder_id, None)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_add_invalid_folder(self):
        self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.assertRaises(
            ValueError,
            self.interface.add_folder,
            USER_NAME, FOLDER_NAME
        )

    def test_delete_folder(self):
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_folder(
            USER_NAME, SUBFOLDER_NAME, 
            parent_folder_id=folder_id
        )

        folders = self.interface.get_folders(USER_NAME)
        self.assertEqual(len(folders), 2)

        self.interface.delete_folder(
            USER_NAME, folder_id, 
            with_subfolders_tree=True
        )
        folders = self.interface.get_folders(USER_NAME)
        self.assertEqual(len(folders), 0)

    def test_get_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE, 
            PLANNER_INTERVAL_COUNT, task_id
        )
        planner = self.interface.get_planner(USER_NAME, planner_id)
        self.assertIsNotNone(planner)
        self.assertEqual(planner.id, planner_id)

    def test_get_invalid_planner(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_planner,
            USER_NAME,
            planner_id=INVALID_ID
        )

    def test_create_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE,
            PLANNER_INTERVAL_COUNT, task_id
        )
        planners = self.interface.get_planners(USER_NAME)
        self.assertEqual(len(planners), 1)

        planner = self.interface.get_planner(USER_NAME, planner_id)
        self.assertIsNotNone(planner.start_time)
        self.assertIsNotNone(planner.task_template)

        compared_values = (
            (planner.user, USER_NAME),
            (planner.interval_type, PLANNER_INTERVAL_TYPE),
            (planner.interval_count, PLANNER_INTERVAL_COUNT),
            (planner.end_time, None),
            (planner.total_tasks_count, None),
            (len(planner.created_tasks), 0),
            (planner.created_tasks_count, 0),
            (planner.last_created_time, planner.start_time - planner.interval)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_create_invalid_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.assertRaises(
            TimeError,
            self.interface.create_planner,
            USER_NAME, PLANNER_INTERVAL_TYPE, PLANNER_INTERVAL_COUNT,
            task_id, INVALID_START_TIME, INVALID_END_TIME
        )

    def test_edit_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE,
            PLANNER_INTERVAL_COUNT, task_id
        )

        interval_type = IntervalType.YEARS
        interval_count = PLANNER_INTERVAL_COUNT + 10
        start_time = datetime.now() - relativedelta(days=1)
        end_time = datetime.now() + relativedelta(days=1) 
        name = NEW_OBJECT_NAME
        task_id = self.interface.create_task(USER_NAME, name)
        total_tasks_count = 10
        self.interface.edit_planner(
            USER_NAME, planner_id, interval_type, interval_count, 
            task_id, start_time, end_time, total_tasks_count
        )

        planner = self.interface.get_planner(USER_NAME, planner_id)
        compared_values = (
            (planner.task_template.name, NEW_OBJECT_NAME),
            (planner.start_time, start_time),
            (planner.end_time, end_time),
            (planner.interval_type, interval_type),
            (planner.interval_count, interval_count),
            (planner.total_tasks_count, total_tasks_count)
        )

        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_invalid_edit_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        total_tasks_count = 5
        planner_delta = relativedelta(
            **{
                PLANNER_INTERVAL_TYPE.value: 
                PLANNER_INTERVAL_COUNT * total_tasks_count
            }
        )
        end_time = datetime.now()
        start_time = end_time - planner_delta
        planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE,
            PLANNER_INTERVAL_COUNT, task_id,
            start_time, end_time, total_tasks_count
        )
        self.interface.generate_planners_tasks(USER_NAME)

        total_tasks_count -= 2
        self.assertRaises(
            ValueError,
            self.interface.edit_planner,
            USER_NAME, planner_id, 
            total_tasks_count=total_tasks_count
        )

        start_time = datetime.now()
        self.assertRaises(
            TimeError,
            self.interface.edit_planner,
            USER_NAME, planner_id, 
            start_time=start_time
        )
        
    def test_generate_count_planners_tasks(self):
        task_delta = relativedelta(minutes=5)
        start_time = datetime.now()
        end_time =  start_time + task_delta
        task_id = self.interface.create_task(
            USER_NAME, TASK_NAME,
            start_time, end_time
        )

        total_tasks_count = 5
        planner_delta = relativedelta(
            **{
                PLANNER_INTERVAL_TYPE.value: 
                PLANNER_INTERVAL_COUNT * total_tasks_count
            }
        )
        start_time = datetime.now() - planner_delta
        count_planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE, 
            PLANNER_INTERVAL_COUNT, task_id, start_time,
            total_tasks_count=total_tasks_count
        )

        self.interface.generate_planners_tasks(USER_NAME)

        count_planner = self.interface.get_planner(USER_NAME, count_planner_id)
        created_task = count_planner.created_tasks[0]

        self.assertEqual(count_planner.created_tasks_count, total_tasks_count)
        self.assertEqual(
            relativedelta(created_task.end_time, created_task.start_time),
            task_delta
        )
    
    def test_generate_time_planners_tasks(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        folder_id = self.interface.add_folder(USER_NAME, FOLDER_NAME)
        self.interface.add_task_to_folder(USER_NAME, task_id, folder_id)

        total_tasks_count = 5
        planner_delta = relativedelta(
            **{
                PLANNER_INTERVAL_TYPE.value: 
                PLANNER_INTERVAL_COUNT * total_tasks_count
            }
        )
        end_time = datetime.now()
        start_time = end_time - planner_delta
        time_planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE, PLANNER_INTERVAL_COUNT, 
            task_id, start_time, end_time
        )

        self.interface.generate_planners_tasks(USER_NAME)

        time_planner = self.interface.get_planner(USER_NAME, time_planner_id)
        folder = self.interface.get_folder(USER_NAME, folder_id)

        self.assertEqual(time_planner.created_tasks_count, total_tasks_count + 1)
        self.assertEqual(len(folder.tasks), total_tasks_count + 2)

    def test_delete_planner(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        planner_id = self.interface.create_planner(
            USER_NAME, PLANNER_INTERVAL_TYPE,
            PLANNER_INTERVAL_COUNT, task_id
        )

        planner = self.interface.get_planner(USER_NAME, planner_id)
        task_template_id = planner.task_template.id

        self.interface.delete_planner(USER_NAME, planner_id)
        planners = self.interface.get_planners(USER_NAME)
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_task_template,
            USER_NAME, task_template_id
        )
        self.assertEqual(len(planners), 0)

    def test_get_notification(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        notification_id = self.interface.add_notification(
            USER_NAME, task_id,
            date=datetime.now(), message=NOTIFICATION_MESSAGE
        )

        notification = self.interface.get_notification(
            USER_NAME, notification_id)
        self.assertEqual(notification.message, NOTIFICATION_MESSAGE)

    def test_get_invalid_notification(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_notification,
            USER_NAME,
            notification_id=INVALID_ID
        )

    def test_add_notification(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        notification_id = self.interface.add_notification(
            USER_NAME, task_id,
            date=datetime.now()
        )

        notifications = self.interface.get_notifications(USER_NAME)
        self.assertEqual(len(notifications), 1)

        notification = self.interface.get_notification(
            USER_NAME, notification_id)
        self.assertIsNotNone(notification.task)
        self.assertIsNotNone(notification.date)
        
        compared_values = (
            (notification.user, USER_NAME),
            (notification.message, None),
            (notification.task_id, task_id),
            (notification.shown, False)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)        

    def test_add_invalid_notification(self):
        start_time = datetime(202, 1, 1)
        end_time=datetime(2020, 2, 2)
        task_id = self.interface.create_task(
            USER_NAME, TASK_NAME,
            start_time=start_time, end_time=end_time
        )

        self.assertRaises(
            TimeError,
            self.interface.add_notification,
            USER_NAME, task_id,
            date=start_time - relativedelta(days=1)
        )
        self.assertRaises(
            TimeError,
            self.interface.add_notification,
            USER_NAME, task_id,
            date=end_time + relativedelta(days=1)
        )

    def test_set_missed_notifications_as_shown(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        notification_id = self.interface.add_notification(
            USER_NAME, task_id,
            date=datetime.now(),
        )

        self.interface.set_missed_notifications_as_shown(USER_NAME)
        
        notification = self.interface.get_notification(
            USER_NAME, notification_id)
        self.assertTrue(notification.shown)

    def test_delete_notification(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        notification_id = self.interface.add_notification(
            USER_NAME, task_id,
            date=datetime.now(),
        )

        self.interface.delete_notification(USER_NAME, notification_id)

        task = self.interface.get_task(USER_NAME, task_id)
        self.assertEqual(len(task.notifications), 0)

    def test_get_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        tag_id = self.interface.add_tag(USER_NAME, TAG_NAME, task_id)

        tag = self.interface.get_tag(USER_NAME, tag_id)
        self.assertEqual(tag.name, TAG_NAME)

    def test_get_invalid_tag(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_tag,
            USER_NAME,
            tag_id=INVALID_ID
        )

    def test_add_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        tag_id = self.interface.add_tag(USER_NAME, TAG_NAME, task_id)

        tags = self.interface.get_tags(USER_NAME)
        self.assertEqual(len(tags), 1)

        task = self.interface.get_task(USER_NAME, task_id)
        self.assertEqual(len(task.tags), 1)

        tag = self.interface.get_tag(USER_NAME, tag_id)
        compared_values = (
            (tag.user, USER_NAME),
            (tag.name, TAG_NAME),
            (len(tag.tasks), task_id)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)  

    def test_add_invalid_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_tag(USER_NAME, TAG_NAME, task_id)

        self.assertRaises(
            AttachError,
            self.interface.add_tag,
            USER_NAME, TAG_NAME, task_id
        )

    def test_edit_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        tag_id = self.interface.add_tag(USER_NAME, TAG_NAME, task_id)

        self.interface.edit_tag(USER_NAME, tag_id, NEW_OBJECT_NAME)

        tag = self.interface.get_tag(USER_NAME, tag_id)
        self.assertEqual(tag.name, NEW_OBJECT_NAME)

    def test_invalid_edit_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        tag_id = self.interface.add_tag(USER_NAME, TAG_NAME, task_id)
        self.interface.add_tag(USER_NAME, NEW_OBJECT_NAME, task_id)

        self.assertRaises(
            ValueError,
            self.interface.edit_tag,
            USER_NAME, tag_id, NEW_OBJECT_NAME
        )

    def test_delete_tag(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        tag_id = self.interface.add_tag(USER_NAME, TAG_NAME, task_id)

        self.interface.delete_tag(USER_NAME, tag_id)

        task = self.interface.get_task(USER_NAME, task_id)
        self.assertEqual(len(task.tags), 0)

    def test_get_executor(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        executor_id = self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, task_id)

        executor = self.interface.get_executor(
            EXECUTOR_USER_NAME, executor_id)
        self.assertEqual(executor.user, EXECUTOR_USER_NAME)

    def test_get_invalid_executor(self):
        self.assertRaises(
            ObjectNotFoundError,
            self.interface.get_executor,
            EXECUTOR_USER_NAME,
            executor_id=INVALID_ID
        )

    def test_delete_executor(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_executor(USER_NAME, EXECUTOR_USER_NAME, task_id)

        self.interface.delete_executor(
            USER_NAME, EXECUTOR_USER_NAME, task_id)
        task = self.interface.get_task(USER_NAME, task_id)
        self.assertEqual(len(task.executors), 0)

    def test_delete_invalid_executor(self):
        first_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        second_task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_executor(
            USER_NAME, EXECUTOR_USER_NAME, first_task_id)

        self.assertRaises(
            DetachError,
            self.interface.delete_executor,
            USER_NAME, USER_NAME, first_task_id
        )
        self.assertRaises(
            DetachError,
            self.interface.delete_executor,
            USER_NAME, INVALID_EXECUTOR_USER_NAME, first_task_id
        )
        self.assertRaises(
            DetachError,
            self.interface.delete_executor,
            USER_NAME, EXECUTOR_USER_NAME, second_task_id
        )

    def test_add_executor(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_executor(USER_NAME, EXECUTOR_USER_NAME, task_id)

        task_executors = self.interface.get_executors(USER_NAME, task_id)
        self.assertEqual(len(task_executors), 1)

    def test_add_invalid_executor(self):
        task_id = self.interface.create_task(USER_NAME, TASK_NAME)
        self.interface.add_executor(USER_NAME, EXECUTOR_USER_NAME, task_id)
        
        self.assertRaises(
            AttachError,
            self.interface.add_executor,
            USER_NAME, EXECUTOR_USER_NAME, task_id
        )
        self.assertRaises(
            AttachError,
            self.interface.add_executor,
            USER_NAME, USER_NAME, task_id
        )

    def test_get_tasks_by_status(self):
        self.interface.create_task(USER_NAME, TASK_NAME)
        tasks = self.interface.get_tasks(USER_NAME, status=Status.IN_PROGRESS)
        self.assertEqual(len(tasks), 1)


if __name__ == "__main__":
    unittest.main()