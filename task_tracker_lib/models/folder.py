"""
    This module contains folder model that used to group 
    tasks and create convenient hierarchy
"""

from sqlalchemy import (
    Column, 
    Integer, 
    String,  
    ForeignKey
)
from sqlalchemy.orm import (
    relationship,
    backref
)

from task_tracker_lib.models.base_model import BaseModel


class Folder(BaseModel):
    """Folder model

    Used to group tasks

    Attributes:
        id, user - inherited from ВaseModel 

        name - folder name
        tasks - tasks contained in the folder

        parent_folder_id - id for relationship with parent folder
        subfolders - subfolders of folder
    """
    __tablename__ = 'folder'

    name = Column(String)
    
    tasks = relationship("Task", back_populates="folder")
    
    parent_folder_id = Column(Integer, ForeignKey('folder.id'))
    subfolders = relationship(
        "Folder",
        backref=backref('parent', remote_side="Folder.id"),
        lazy="joined"
    )
    
    def __init__(self, user, name, group_id=None):
        super(Folder, self).__init__(user)

        self.name = name

    def __str__(self):
        tasks_ids = ', '.join(str(task.id) for task in self.tasks)
        subfolders_ids = (
            ', '.join(
                str(subfolders.id) for subfolders in self.subfolders
            )
        )

        return '\n '.join([
            "Folder id={0} name={1}:",
            "subfolders_ids - [{2}]",
            "tasks_ids - [{3}]"
        ]).format(
            self.id,
            self.name, 
            tasks_ids if self.tasks else "no tasks",
            subfolders_ids if self.subfolders else "no subfolders"
        )
        
    def __repr__(self):
        return "<Folder(id={self.id}, name={self.name})>".format(self=self)