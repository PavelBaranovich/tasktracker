"""
    This module contains tag model that used for grouping 
    tasks by themes and task_tag_association table for 
    relationship with task
"""

from sqlalchemy import (
    Table,
    Column, 
    Integer, 
    String,  
    ForeignKey
)
from sqlalchemy.orm import relationship

from task_tracker_lib.models.base_model import BaseModel


task_tag_association = Table(
    'task_tag', 
    BaseModel.metadata,
    Column('task_id', Integer, ForeignKey('task.id')),
    Column('tag_id', Integer, ForeignKey('tag.id'))
)


class Tag(BaseModel):
    """Tag model

    Attributes:
        id, user - inherited from ВaseModel 

        name - tag name
        tasks - tasks that contain tag
    """
    __tablename__ = 'tag'
    
    name = Column(String)
    
    tasks = relationship(
        "Task",
        secondary=task_tag_association,
        back_populates="tags"
    )
    
    def __init__(self, user, name):
        super(Tag, self).__init__(user)

        self.name = name
            
    def __str__(self):
        return "Tag id={self.id}, name={self.name}".format(self=self)

    def __repr__(self):
        return "<Tag(id={self.id}, name={self.name})>".format(self=self)