"""
    This module contains planner model that used for creates 
    tasks at specific time interval and enums corresponding 
    to them (class IntervalType)
"""

import enum
from datetime import datetime
from dateutil.relativedelta import relativedelta

from sqlalchemy import (
    Column, 
    Integer, 
    DateTime, 
    ForeignKey,
    Enum
)
from sqlalchemy.orm import relationship

from task_tracker_lib.models.base_model import BaseModel
from task_tracker_lib.models.task import PRINTABLE_DATE_FORMAT


class IntervalType(enum.Enum):
    """Enum for storing planner interval type"""
    MINUTES = "minutes"
    HOURS = "hours"
    DAYS = "days"
    WEEKS = "weeks"
    MOUNTHS = "months"
    YEARS = "years"

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns IntervalType object corresponding to string"""
        return IntervalType[string]


class Planner(BaseModel):
    """Planner model

    Attributes:
        id, user - inherited from ВaseModel 

        start_time - time after which task creation begins
        end_time - time after which task creation ends
        interval_type - type of interval between tasks creation
        interval_count - count of planner interterval type
        last_created_time - last time in which task was created
        total_tasks_count - total number of tasks required to create
        created_tasks_count - count of tasks created by planner

        task_template_id - id for relationship with task template
        task_template - template for creating new tasks

        created_tasks - list of tasks created by planner
    """
    __tablename__ = 'planner'

    start_time = Column(DateTime)
    end_time = Column(DateTime)
    interval_type = Column(Enum(IntervalType))
    interval_count = Column(Integer)
    last_created_time = Column(DateTime)
    total_tasks_count = Column(Integer)
    created_tasks_count = Column(Integer)
    
    task_template_id = Column(Integer, ForeignKey('task_template.id'))
    task_template = relationship("TaskTemplate", back_populates="planner")

    created_tasks = relationship("Task", back_populates="planner")
    
    def __init__(self, 
                 user,
                 interval_type,
                 interval_count,
                 task_template,
                 start_time=None, 
                 end_time=None, 
                 total_tasks_count=None):
        super(Planner, self).__init__(user)

        if start_time is not None:
            self.start_time = start_time
        else:
            self.start_time = datetime.now()

        self.end_time = end_time
        self.interval_type = interval_type
        self.interval_count = interval_count
        self.task_template = task_template
        self.total_tasks_count = total_tasks_count
        self.created_tasks_count = 0
        self.last_created_time = start_time - self.interval
        
    @property
    def interval(self):
        return relativedelta(**{self.interval_type.value: self.interval_count})

    @property
    def status(self):
        if self.created_tasks_count == 0:
            return "IN_WAITING"
        if (self.total_tasks_count is not None and 
                self.created_tasks_count == self.total_tasks_count):
            return "FINISHED"
        if (self.end_time is not None and 
                self.last_created_time + self.interval > self.end_time):
            return "FINISHED"
        return "ACTIVE"
     
    def __str__(self):
        start_time_string = self.start_time.strftime(PRINTABLE_DATE_FORMAT)
        created_tasks_ids = (
            ', '.join(
                str(task.id) for task in self.created_tasks
            )
        )
        if self.end_time is not None:
            end_time_string = self.end_time.strftime(PRINTABLE_DATE_FORMAT)
        if self.last_created_time < self.start_time:
            last_created_time_string = None
        else:
            last_created_time_string = (self.last_created_time
                                        .strftime(PRINTABLE_DATE_FORMAT))

        return '\n '.join([
            "Planner id={0}:",
            "status - {1}",
            "start time - {2}",
            "end time - {3}",
            "interval type - {4}",
            "interval count - {5}",
            "total tasks count - {6}",
            "created tasks ids - [{7}]",
            "task template id - {8}",
            "last task creation time - {9}"
        ]).format(
            self.id, 
            self.status,
            start_time_string,
            end_time_string if self.end_time else "no end time",
            self.interval_type,
            self.interval_count,
            self.total_tasks_count,
            created_tasks_ids if self.created_tasks else "no tasks",
            self.task_template_id,
            last_created_time_string
        )

    def __repr__(self):
        return "<Planner(id={self.id})>".format(self=self)