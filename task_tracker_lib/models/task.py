"""
    This module contains task model and task template model (used by planner),
    and enums corresponding to them (class Priority and class Status)
"""

import enum
from datetime import datetime

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import (
    Column, 
    Integer, 
    String, 
    DateTime, 
    ForeignKey, 
    Enum
)
from sqlalchemy.orm import (
    backref, 
    relationship
)

from task_tracker_lib.models.base_model import BaseModel
from task_tracker_lib.models.tag import task_tag_association
from task_tracker_lib.models.executor import task_executor_association

PRINTABLE_DATE_FORMAT = "%d %B %Y  %H:%M"


class Priority(enum.Enum):
    """Enum for storing task priority"""

    HIGH = 1
    NORMAL = 2
    LOW = 3

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns Priority object corresponding to string"""
        return Priority[string]
    
    
class Status(enum.Enum):
    """Enum for storing task status"""

    TODO = 1
    IN_PROGRESS = 2
    DONE = 3
    FAILED = 4

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns Status object corresponding to string"""
        return Status[string]


class Task(BaseModel):
    """Task model

    Attributes:
        id, user - inherited from ВaseModel 

        name - task name
        tree_id - common id for tasks in task tree.
                   Used to prevent loops in tree
        _status - task's execution status
        start_time - start time of task execution
        end_time - time after which task will fail
        description - detailed description of task
        priority - importance of task 

        parent_task_id - id for relationship with parent task
        subtasks - subtasks of task

        executors - users (except creator) who can execute (complete) task
        tags - task themes (categories)
        notifications - reminder of uncompleted task

        planner_id - id for relationship with planner
        planner - planner that created task

        folder_id - id for relationship with folder
        folder - folder in which the task is stored
    """
    __tablename__ = 'task'

    name = Column(String)
    tree_id = Column(Integer)
    _status = Column('status', Enum(Status))
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    description = Column(String)
    priority = Column(Enum(Priority))
    
    parent_task_id = Column(Integer, ForeignKey('task.id'))
    subtasks = relationship(
        "Task",
        backref=backref('parent', remote_side="Task.id"),
        lazy="joined"
    )

    executors = relationship(
        "Executor",
        secondary=task_executor_association,
        back_populates="tasks"
    )
    
    tags = relationship(
        "Tag",
        secondary=task_tag_association,
        back_populates="tasks"
    )

    notifications = relationship("Notification", back_populates="task")
    
    planner_id = Column(Integer, ForeignKey('planner.id'))
    planner = relationship("Planner", back_populates="created_tasks")

    folder_id = Column(Integer, ForeignKey('folder.id'))
    folder = relationship("Folder", back_populates="tasks")
    
    def __init__(self,
                 user,
                 name,
                 tree_id=None,
                 start_time=None,
                 end_time=None,
                 priority=None,
                 status=None,
                 description=None):
        super(Task, self).__init__(user)

        self.name = name     
        self.tree_id = tree_id  
        self.priority = priority
        self.description = description if description is not None else ""
        self.start_time = start_time if start_time is not None else datetime.now()
        self.end_time = end_time
        self.status = status if status is not None else Status.TODO
        self.priority = priority if priority is not None else Priority.LOW

    @hybrid_property
    def status(self):
        if self._status == Status.TODO and self.start_time < datetime.now():
                self._status = Status.IN_PROGRESS
        if self.end_time is not None and self._status == Status.IN_PROGRESS:
            if self.end_time < datetime.now():
                self.status = Status.FAILED

        return self._status

    @status.setter
    def status(self, value):
        self._status = value
     
    def __str__(self):
        if self.end_time is not None:
            end_time_string = self.end_time.strftime(PRINTABLE_DATE_FORMAT)
        executors_names = (
            ', '.join(
                executor.user for executor in self.executors
            )
        )
        if self.folder is not None:
            folder_name = self.folder.name
        tags_names = ', '.join(tag.name for tag in self.tags)
        start_time_string = self.start_time.strftime(PRINTABLE_DATE_FORMAT)
        subtasks_ids = (
            ', '.join(
                str(subtasks.id) for subtasks in self.subtasks
            )
        )

        return '\n '.join([
            "Task id={0} name={1}:",
            "start time - {2}",
            "end time - {3}",
            "subtasks ids - [{4}]",
            "description - {5}",
            "status - {6}",
            "priority - {7}",
            "folder - {8}",
            "tags - [{9}]",
            "executors - [{10}]"
        ]).format(
            self.id,
            self.name, 
            start_time_string,
            end_time_string if self.end_time else "no end time",
            subtasks_ids if self.subtasks else "no subtasks",
            self.description if self.description else "no description",
            self.status.name,
            self.priority.name,
            folder_name if self.folder is not None else "no folder",
            tags_names if self.tags else "no tags",
            executors_names if self.executors else "no executors"
        )

    def __repr__(self):
        return "<Task(id={self.id} name={self.name})>".format(self=self)


class TaskTemplate(BaseModel):
    """TaskTemplate model

    Used as template for new tasks created by planner

    Attributes:
        id, user - inherited from ВaseModel 

        name - task name
        start_time - the start time of task execution
        end_time - time after which task will fail
        description - detailed description of task
        priority - importance of task 
        planner - planner that uses task template for new 
                  tasks creating
        folder_id - id for relationship with folder
    """
    __tablename__ = 'task_template'

    name = Column(String)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    description = Column(String)
    priority = Column(Enum(Priority))
    folder_id = Column(Integer)
    
    planner = relationship(
        "Planner", 
        uselist=False, 
        back_populates="task_template"
    )
    
    def __init__(self,
                 user,
                 name,
                 start_time=None,
                 end_time=None,
                 priority=None,
                 description=None,
                 folder_id=None):
        super(TaskTemplate, self).__init__(user)

        self.name = name       
        self.priority = priority
        self.description = description
        self.start_time = start_time
        self.end_time = end_time
        self.priority = priority
        self.folder_id = folder_id
     
    def __str__(self):
        start_time_string = self.start_time.strftime(PRINTABLE_DATE_FORMAT)
        if self.end_time is not None:
            end_time_string = self.end_time.strftime(PRINTABLE_DATE_FORMAT)

        return '\n '.join([
            "TaskTemplate id={0} name={1}:",
            "start time - {2}",
            "end time - {3}",
            "description - {4}",
            "priority - {5}",
            "folder id - {6}"
        ]).format(
            self.id,
            self.name, 
            start_time_string,
            end_time_string if self.end_time else "no end time",
            self.description if self.description else "no description",
            self.priority.name,
            self.folder_id
        )

    def __repr__(self):
        return "<TaskTemplate(id={self.id} name={self.name})>".format(self=self)