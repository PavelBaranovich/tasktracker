"""
    This module contains executor model that used for assigning 
    rights to execute tasks to other users and 
    task_executor_association table for relationship with task
"""
from sqlalchemy import (
    Table,
    Column, 
    Integer, 
    ForeignKey
)
from sqlalchemy.orm import relationship

from task_tracker_lib.models.base_model import BaseModel


task_executor_association = Table(
    'task_executor', 
    BaseModel.metadata,
    Column('task_id', Integer, ForeignKey('task.id')),
    Column('executor_id', Integer, ForeignKey('executor.id'))
)


class Executor(BaseModel):
    """Executor model

    Represents user who can execute other user's tasks

    Attributes:
        id, user - inherited from ВaseModel 

        tasks - tasks that user has right to execute
    """
    __tablename__ = 'executor'
    
    tasks = relationship(
        "Task",
        secondary=task_executor_association,
        back_populates="executors"
    )
    
    def __init__(self, user):
        super(Executor, self).__init__(user)
            
    def __str__(self):
        return "Executors id={self.id}, user={self.user}".format(self=self)

    def __repr__(self):
        return "<Executor(id={self.id}, user={self.user})>".format(self=self)