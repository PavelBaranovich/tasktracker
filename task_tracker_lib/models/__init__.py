"""
    This package contains database models used in library

    Modules:
        base_model.py - contains model that describes common 
                        characteristics of models
        foldel.py - describes model for grouping tasks
        notification.py - describes notification model
        executor.py - describes model for assigning rights to execute 
                      tasks to other users and relationship table 
                      between task and executor
        planner.py - describes model that creates tasks at specific 
                     time interval and enums corresponding to them
        tag.py - describes model for grouping tasks by themes and 
                 relationship table between task and tag
        task.py - describes task model (main application model) and 
                  task template model (for create tasks by planner), 
                  and enums corresponding to them
"""