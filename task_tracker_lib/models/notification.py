"""
    This module contains notification model that used 
    to remind you of tasks to be executed
"""

from sqlalchemy import (
    Column, 
    Integer, 
    String,  
    ForeignKey,
    DateTime,
    Boolean
)
from sqlalchemy.orm import relationship

from task_tracker_lib.models.base_model import BaseModel
from task_tracker_lib.models.task import PRINTABLE_DATE_FORMAT


class Notification(BaseModel):
    """Notification model

    Attributes:
        id, user - inherited from ВaseModel 

        date - time when notification should be shown
        message - notification message
        shown - flag indicates whether notification was shown
    
        task_id - id for relationship with task
        task - task for which notification should be shown
    """
    __tablename__ = 'notification'

    date = Column(DateTime)
    message = Column(String)
    shown = Column(Boolean)
    
    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship("Task", back_populates="notifications")
    
    def __init__(self, user, date, message=None):
        super(Notification, self).__init__(user)

        self.message = message
        self.date = date
        self.shown = False

    def __str__(self):
        date_string = self.date.strftime(PRINTABLE_DATE_FORMAT)

        return '\n '.join([
            "Notification id={0}:",
            "message - {1}",
            "date - {2}",
            "task id - {3}",
            "is shown - {4}"
        ]).format(
            self.id, 
            self.message or "default",
            date_string,
            self.task.id,
            self.shown
        )
        
    def __repr__(self):
        return "<Notification(id={self.id})>".format(self=self)