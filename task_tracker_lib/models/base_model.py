"""
    This module contains common model for all entities in library
"""

from sqlalchemy import (
    Column,
    Integer,
    String
)

from task_tracker_lib.database import Base


class BaseModel(Base):
    """Base model class

    Used as common model for all entities in library.

    Attributes:
        id (int): unique models identifier
        user (str): model creator (owner) 
    """
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    user = Column(String)

    def __init__(self, user):
        self.user = user