"""
    This module contains decorator that is used to record information
    about function call and errors to log file, and get_logger function
    to obtain logger by unique name
"""

import logging
from functools import wraps

LIBRARY_LOGGER_NAME = "python_tracker_logger"


def log_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = get_logger()
        try:
            logger.info("called function: {}".format(func.__name__))
            logger.debug("args: {}".format(args))
            logger.debug("kwargs: {}".format(kwargs))
            result = func(*args, **kwargs)
            logger.debug("returned: {!r}".format(result))

            return result
        except Exception as e:
            logger.exception(e)
            raise

    return wrapper


def get_logger():
    return logging.getLogger(LIBRARY_LOGGER_NAME)