"""
    This package contains api for interacting with database models

    Modules:
        exceptions.py - describes some exceptions that can occur 
                        when working with database
        interface.py - describes logic of interacting with database
                       models
        logger.py - describes decorator that is used to record 
                    information about function call and errors to log 
                    file, and function to obtain logger by unique name
"""