"""
    This module contains exception classes used by library
"""

class TrackerLibraryError(Exception):
    """Main exception class in the library"""


class ObjectNotFoundError(TrackerLibraryError):
    """Exception reporting the absence of an object in the database"""


class TimeError(TrackerLibraryError):
    """Exception that reporting about invalid time operation"""


class AttachError(TrackerLibraryError):
    """Exception that reporting about error establishing link between objects"""


class DetachError(TrackerLibraryError):
    """Exception that reporting about error breaking links between objects"""


class LoopbackError(AttachError):
    """Exception that reporting about loop in task tree"""


class StatusError(TrackerLibraryError):
    """Exception that reporting about incorrect actions on object with certain status"""