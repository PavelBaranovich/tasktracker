"""
    This module contains interface class that is used to get all 
    functionality of tracker library and establish connection to database
"""

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

import sqlalchemy
from sqlalchemy import (
    and_,
    or_
)

from task_tracker_lib.models.folder import Folder
from task_tracker_lib.models.notification import Notification
from task_tracker_lib.models.executor import Executor
from task_tracker_lib.models.planner import Planner
from task_tracker_lib.models.tag import Tag
from task_tracker_lib.database import get_session
from task_tracker_lib.api.logger import log_decorator
from task_tracker_lib.models.task import (
    Task, 
    TaskTemplate,
    Priority,
    Status
)
from task_tracker_lib.api.exceptions import ( 
    ObjectNotFoundError,
    TimeError,
    AttachError,
    DetachError,
    LoopbackError,
    StatusError
)


class Interface:
    """Class of interaction with database models

    Main features of class:
        1.  Сreating objects from appropriate data set
        2.  Getting objects by their unique id
        3.  Editing objects
        4.  Getting tasks and folders tree as nested list
        5.  Attaching and detaching subtasks
        6.  Getting list of objects by appropriate filters
        7.  Deleting objects by their unique id
        8.  Getting list of missed notifications
        9.  Generation of planners tasks that should have been 
            created before the program was called
        10. Additing and deleting tasks to/from folders 
    """

    def __init__(self, database_url):
        """Initializes database connection

        :param database_url: path to database and its settings
        """
        self._session = get_session(database_url)

    def __del__(self):
        """Removes database connection"""
        if hasattr(self, "_session") and self._session is not None:
            self._session.close()    

    @log_decorator
    def get_task(self, user, task_id, as_executor=False):
        """Returns task by id or raises exception if it doesn't exist

        :param task_id: id of received task
        :param as_executor: flag of task receipt on behalf of owner
                            or executor 
        :return: task object
        """
        query = self._session.query(Task)
        if as_executor:
            query = query.join(Task.executors).filter(Executor.user == user)
        else:
            query = query.filter(Task.user == user)

        try:
            return query.filter(Task.id == task_id).one()
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Task not found")

    @log_decorator
    def create_task(self, 
                    user,
                    name, 
                    start_time=None, 
                    end_time=None, 
                    priority=None,
                    description=None):
        """Сreates task from appropriate data set

        :param user: task owner
        :param name: task name
        :param start_time: start time of task execution
        :param end_time: time after which task will fail
        :param priority: task priority (Priority class object)
        :param description: detailed description of task
        :return: new task id
        """
        if start_time is None:
            start_time = datetime.now()

        if end_time is not None:
            if start_time > end_time:
                raise TimeError("Start time more than end time")
            elif end_time < datetime.now() - relativedelta(minutes=1):
                raise TimeError("End time less than present time")                

        status = Status.TODO
        if start_time <= datetime.now():
            status = Status.IN_PROGRESS

        if priority is None:
            priority = Priority.LOW

        if description is None:
            description = ""

        task = Task(
            user=user,
            name=name,
            start_time=start_time,
            end_time=end_time,
            priority=priority,
            description=description,
            status=status
        )

        self._save(task)

        return task.id

    @log_decorator
    def get_tasks(self, 
                  user,
                  name=None,
                  status=None,
                  tree_id=None,
                  start_time=None, 
                  end_time=None, 
                  priority=None,
                  description=None,
                  parent_task_id=None,
                  planner_id=None,
                  folder_id=None,
                  without_folder=False,
                  without_parent_task=False,
                  as_executor=False):
        """Returns list of tasks that match specified filters"""
        filter_condition = and_(
            or_(tree_id is None, Task.tree_id == tree_id),
            or_(start_time is None, Task.start_time == start_time),
            or_(end_time is None, Task.end_time == end_time),
            or_(priority is None, Task.priority == priority),
            or_(parent_task_id is None, Task.parent_task_id == parent_task_id),
            or_(planner_id is None, Task.planner_id == planner_id),
            or_(folder_id is None, Task.folder_id == folder_id),
            or_(not without_folder, Task.folder_id == None),
            or_(not without_parent_task, Task.parent_task_id == None)
        )

        query = self._session.query(Task)
        if as_executor:
            query = query.join(Task.executors).filter(Executor.user == user)
        else:
            query = query.filter(Task.user == user)

        if name is not None:
            query = query.filter(Task.name.ilike("%" + name + "%"))

        if description is not None:
            query = query.filter(Task.name.ilike("%" + description + "%"))

        filtered_tasks = query.filter(filter_condition).all()
        if status is not None:
            tasks = filtered_tasks
            filtered_tasks = []
            for task in tasks:
                if task.status == status:
                    filtered_tasks.append(task)

        self._session.commit()
        return filtered_tasks

    @log_decorator
    def get_task_tree(self, user, task_id):
        """Returns task tree as nested list"""
        task = self.get_task(user, task_id)
        return self._get_task_tree(task)

    def _get_task_tree(self, task):
        task_tree = [task]
        for subtask in task.subtasks:
            subtasks_tree = self._get_task_tree(subtask)
            task_tree.append(subtasks_tree)

        return task_tree

    @log_decorator
    def complete_task(self, user, task_id):
        """Сhanges status of task to completed"""
        task = self._session.query(Task).filter_by(id=task_id).first()
        if task is None or not self._check_rigth_to_execute_task(user, task):
            raise ObjectNotFoundError("Task not found")
        if task.status != Status.IN_PROGRESS:
            raise StatusError("Task isn't in progress status")

        for notification in task.notifications:
            if not notification.shown:
                self._delete(notification)

        task.status = Status.DONE
        self._complete_subtasks(task)

        self._session.commit()

    def _check_rigth_to_execute_task(self, user, task):
        """Сhecks right to complete task"""
        if task.user == user:
            return True

        for executor in task.executors:
            if executor.user == user:
                return True

        return False

    def _complete_subtasks(self, task):
        """Сhanges status of subtasks to completed"""
        for subtask in task.subtasks:
            subtask.status = Status.DONE
            self._complete_subtasks(subtask)

    @log_decorator
    def attach_subtask(self, user, task_id, subtask_id, with_executors=False):
        """Adds parent task to specified task"""
        task = self.get_task(user, task_id)
        subtask = self.get_task(user, subtask_id)
        
        if task.status == Status.FAILED or subtask.status == Status.FAILED:
            raise StatusError("Task has a failed status")
        if subtask.parent_task_id is not None:
            raise AttachError("Subtask already has a parent task")
        if (task.tree_id == subtask.id or subtask.tree_id == task.id or 
                task_id == subtask_id):
            raise LoopbackError("Relationships form a cycle")
        if subtask.end_time is not None and task.start_time > subtask.end_time:
            raise TimeError("Subtask ends before task")
        if task.end_time is not None and subtask.start_time > task.end_time:
            raise TimeError("Task ends before subtask")
        if task.folder_id != subtask.folder_id:
            raise AttachError("Task and subtask have different locations")

        task.subtasks.append(subtask)
        if with_executors:
            for executor in task.executors:
                self._add_subtasks_executor(task, executor)

        new_subtask_tree_id = task.id
        if task.tree_id is not None:
            new_subtask_tree_id = task.tree_id
        self._change_tree_id(subtask, new_subtask_tree_id)

        self._session.commit()


    @log_decorator
    def detach_subtask(self, user, subtask_id):
        """Deletes parent task from specified task"""
        subtask = self.get_task(user, subtask_id)
        if subtask.parent_task_id is None:
            raise DetachError("Task doesn't have parent task")
        else:
            parent_task = self.get_task(user, subtask.parent_task_id)
            parent_task.subtasks.remove(subtask)

            self._change_tree_id(subtask, subtask.id)

        self._session.commit()

    @log_decorator
    def delete_task(self, user, task_id, with_task_tree=True):
        """Deletes task together with its tree"""
        task = self.get_task(user, task_id)

        for notification in task.notifications:
            self._delete(notification)

        for subtask in task.subtasks:
            if with_task_tree:
                self.delete_task(user, subtask.id, with_task_tree=True)
            else:
                self._change_tree_id(subtask, subtask.id)

        for tag in task.tags:
            if len(tag.tasks) == 1:
                self._delete(tag)

        for executor in task.executors:
            if len(executor.tasks) == 1:
                self._delete(executor)

        self._delete(task)

    @log_decorator
    def edit_task(self, 
                  user,
                  task_id,
                  name=None, 
                  start_time=None, 
                  end_time=None, 
                  priority=None,
                  description=None):
        """Edits certain fields of task"""
        task = self.get_task(user, task_id)

        if name is not None:
            task.name = name

        if start_time is not None:
            task.start_time = start_time

            if start_time < datetime.now():
                task.status = Status.IN_PROGRESS
            else:
                task.status = Status.TODO

        if end_time is not None:
            if task.start_time > end_time:
                raise TimeError("Start time more than end time")
            elif end_time < datetime.now()  - relativedelta(minutes=1):
                raise TimeError("End time less than present time") 
            else:
                task.end_time = end_time

        if priority is not None:
            task.priority = priority

        if description is not None:
            task.description = description

        self._session.commit()

    def _change_tree_id(self, task, tree_id):
        """Сhanges tree id in task tree"""
        task.tree_id = tree_id
        for subtask in task.subtasks:
            self._change_tree_id(subtask, tree_id)

    @log_decorator
    def get_subtasks(self, user, task_id):
        """Returns list of subtasks for parent task"""
        task = self.get_task(user, task_id)
        
        return task.subtasks

    @log_decorator
    def add_task_to_folder(self, user, task_id, folder_id):
        """Adds task with subtasks to folder"""
        task = self.get_task(user, task_id)
        folder = self.get_folder(user, folder_id)

        if task.folder_id == folder_id:
            raise AttachError("Task is already in this folder")
        if task.parent_task_id is not None:
            raise AttachError("Subtask cannot be added without parent task")

        task.folder = folder
        self._add_subtasks_to_folder(task, folder)

        self._session.commit()

    def _add_subtasks_to_folder(self, task, folder):
        """Adds subtasks to folder"""
        for subtask in task.subtasks:
            subtask.folder = folder
            self._add_subtasks_to_folder(subtask, folder)

    @log_decorator
    def delete_task_from_folder(self, user, task_id):
        """Deletes task with subtasks from folder"""
        task = self.get_task(user, task_id)
        folder = task.folder

        if task.folder_id is None:
            raise DetachError("Task doesn't have a folder")
        if task.parent_task_id is not None:
            raise DetachError("Subtask cannot be deleted without parent task")
        
        folder.tasks.remove(task)
        self._delete_subtasks_from_folder(task, folder)

        self._session.commit()

    def _delete_subtasks_from_folder(self, task, folder):
        """Deletes subtasks from folder"""
        for subtask in task.subtasks:
            folder.tasks.remove(subtask)
            self._add_subtasks_to_folder(subtask, folder)

    @log_decorator
    def get_folder(self, user, folder_id):
        """Returns folder by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(Folder)
                    .filter_by(user=user)
                    .filter_by(id=folder_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Folder not found")

    @log_decorator
    def get_folders(self, user, parent_folder_id=None, name=None):
        """Returns list of folders that match specified filters"""
        filter_condition = and_(
            or_(name is None, Folder.name == name),
            or_(
                parent_folder_id is None, 
                Folder.parent_folder_id == parent_folder_id
            )
        )

        return (self._session.query(Folder)
                .filter_by(user=user)
                .filter(filter_condition)
                .all())

    @log_decorator
    def add_folder(self, user, name, parent_folder_id=None):
        """Сreates folder from appropriate data set

        :param user: folder owner
        :param name: folder name
        :param parent_folder_id: id of folder in which new 
                                 folder is created 
        :return: new folder id
        """
        folder = (self._session.query(Folder)
                  .filter_by(user=user)
                  .filter_by(name=name)
                  .first())
        if folder is not None:
            raise ValueError("Folder with specified name already exists")

        folder = Folder(user=user, name=name)

        if parent_folder_id is not None:
            parent_folder = self.get_folder(user, parent_folder_id)
            parent_folder.subfolders.append(folder)

        self._save(folder)

        return folder.id

    @log_decorator
    def delete_folder(self, user, folder_id, with_subfolders_tree=False):
        """Deletes foder together with its tree"""
        folder = self.get_folder(user, folder_id)

        if with_subfolders_tree:
            self._delete_folder_tree(folder)

        self._delete(folder)

    def _delete_folder_tree(self, folder):
        for subfolder in folder.subfolders:
            self._delete_folder_tree(subfolder)
            self._session.delete(subfolder)

    @log_decorator
    def get_folder_tree(self, user, folder_id):
        """Returns folder tree as nested list"""
        folder = self.get_folder(user, folder_id)
        return self._get_folder_tree(folder)

    def _get_folder_tree(self, folder):
        folder_tree = [folder]
        for subfolder in folder.subfolders:
            subfolders_tree = self._get_folder_tree(subfolder)
            folder_tree.append(subfolders_tree)

        return folder_tree

    @log_decorator
    def edit_folder(self, user, folder_id, name=None):
        """Edits certain fields of folder"""
        folder = self.get_folder(user, folder_id)

        if name is not None:
            if self.get_folders(user, name=name) and name != folder.name:
                raise ValueError("Folder with specified name already exists")
            folder.name = name

        self._session.commit()

    @log_decorator
    def get_planner(self, user, planner_id):
        """Returns planner by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(Planner)
                    .filter_by(user=user)
                    .filter_by(id=planner_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Planner not found")

    @log_decorator
    def get_planners(self, 
                     user,
                     interval_type=None,
                     interval_count=None,
                     last_created_time=None,
                     total_tasks_count=None,
                     created_tasks_count=None):
        """Returns list of planners that match specified filters"""
        filter_condition = and_(
            Planner.user == user,
            or_(
                interval_type is None, 
                Planner.interval_type == interval_type
            ),
            or_(
                interval_count is None, 
                Planner.interval_count == interval_count
            ),
            or_(
                last_created_time is None, 
                Planner.last_created_time == last_created_time
            ),
            or_(
                total_tasks_count is None, 
                Planner.total_tasks_count == total_tasks_count
            ),
            or_(
                created_tasks_count is None, 
                Planner.created_tasks_count == created_tasks_count
            )
        )

        return self._session.query(Planner).filter(filter_condition).all()

    @log_decorator
    def create_planner(self, 
                       user,
                       interval_type,
                       interval_count,
                       task_id,
                       start_time=None, 
                       end_time=None, 
                       total_tasks_count=None):
        """Сreates planner from appropriate data set

        :param user: planner owner
        :param interval_type: type of interval between tasks creation
                              (IntervalType class object)
        :param interval_count: count of planner interterval type
        :param task_id: id of task from which template is created
        :param start_time: time after which task creation begins
        :param end_time: time after which task creation ends
        :param total_tasks_count: total number of tasks required to create
        :return: new planner id
        """
        if start_time is None:
            start_time = datetime.now()

        if end_time is not None:    
            if start_time > end_time:
                raise TimeError("Start time more than end time")

        task = self.get_task(user, task_id)
        task_template = self._create_task_template(user, task)

        planner = Planner(
            user=user,
            interval_type=interval_type,
            interval_count=interval_count,
            task_template=task_template,
            start_time=start_time,
            end_time=end_time,
            total_tasks_count=total_tasks_count
        )

        self._save(planner)

        return planner.id

    @log_decorator
    def edit_planner(self, 
                     user,
                     planner_id,
                     interval_type=None,
                     interval_count=None,
                     task_id=None,
                     start_time=None, 
                     end_time=None, 
                     total_tasks_count=None):
        """Edits certain fields of planner"""
        planner = self.get_planner(user, planner_id)

        if interval_type is not None:
            planner.interval_type = interval_type

        if interval_count is not None:
            planner.interval_count = interval_count

        if ((start_time is not None) and (end_time is not None) and
                start_time > end_time):
            raise TimeError("New start time more than new end time")

        if start_time is not None:
            if planner.end_time is not None and start_time > planner.end_time:
                raise TimeError("New start time more than planner end time")
            else:
                planner.start_time = start_time
                planner.last_created_time = planner.start_time - planner.interval

        if end_time is not None:
            if planner.start_time > end_time:
                raise TimeError("Planner start time more than new end time")
            else:
                planner.end_time = end_time

        if total_tasks_count is not None:
            if total_tasks_count < planner.created_tasks_count:
                raise ValueError("Total tasks count less then created tasks count")
            else:
                planner.total_tasks_count = total_tasks_count

        if task_id is not None:
            task = self.get_task(user, task_id)
            task_template = planner.task_template
            self._delete(task_template)

            task_template = self._create_task_template(user, task)
            planner.task_template = task_template

        self._session.commit()

    @log_decorator
    def generate_planners_tasks(self, user):
        """Generates tasks that need to be created by planners"""
        planners = self.get_planners(user)
        for planner in planners:
            self._generate_planner_tasks(planner)

        self._session.commit()

    def _generate_planner_tasks(self, planner):
        """Generates tasks that need to be created by planner"""
        task_template = planner.task_template
        folder = None
        if task_template.folder_id is not None:
            folder = (self._session.query(Folder)
                      .filter_by(id=task_template.folder_id)
                      .first())
        
        while self._check_can_create_planner_task(planner):
            task_end_time = task_template.end_time
            task_start_time = planner.last_created_time + planner.interval
            if task_end_time is not None:
                task_time_delta = relativedelta(
                    task_template.end_time,
                    task_template.start_time
                )
                task_end_time = task_start_time + task_time_delta

            planner.last_created_time += planner.interval
            planner.created_tasks_count += 1

            task = Task(
                user=task_template.user, 
                name=task_template.name, 
                start_time=task_start_time, 
                end_time=task_end_time, 
                priority=task_template.priority, 
                status=Status.IN_PROGRESS,
                description=task_template.description
            )

            if folder is not None:
                folder.tasks.append(task)
            planner.created_tasks.append(task)

            self._session.add(task)
            
    def _check_can_create_planner_task(self, planner):
        """Сhecks possibility of creating new task by planner"""
        if planner.start_time > datetime.now():
            return False

        next_create_time = planner.last_created_time + planner.interval
        if (planner.end_time is not None and 
                planner.end_time < next_create_time):
            return False

        next_create_count = planner.created_tasks_count + 1
        if (next_create_time <= datetime.now() and
                (planner.total_tasks_count is None or
                next_create_count <= planner.total_tasks_count)):
            return True

        return False

    @log_decorator
    def delete_planner(self, user, planner_id):
        """Deletes planner by id"""
        planner = self.get_planner(user, planner_id)
        task_template = planner.task_template

        self._delete(task_template)
        self._delete(planner)

    @log_decorator
    def get_notification(self, user, notification_id):
        """Returns notification by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(Notification)
                    .filter_by(user=user)
                    .filter_by(id=notification_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Notification not found")

    @log_decorator
    def get_notifications(self,
                          user,
                          date=None,
                          shown=None,
                          task_id=None,
                          message=None):
        """Returns list of notifications that match specified filters"""
        filter_condition = and_(
            Notification.user == user,
            or_(date is None, Notification.date == date),
            or_(shown is None, Notification.shown == shown),
            or_(task_id is None, Notification.task_id == task_id),
            or_(message is None, Notification.message == message)
        )

        return self._session.query(Notification).filter(filter_condition).all()

    @log_decorator
    def add_notification(self, user, task_id, date, message=None):
        """Сreates notification from appropriate data set

        :param user: notification owner
        :param task_id: id of task for which notification should be shown
        :param date: time when notification should be shown
        :param message: notification message
        :return: new notification id
        """
        task = self.get_task(user, task_id)

        if date < task.start_time:
            raise TimeError("Notification date less than task start time")

        if (date < datetime.now() - relativedelta(minutes=1) and 
                datetime.now() - date > timedelta(seconds=1)):
            raise TimeError("Notification date less than present time")

        if task.end_time is not None:
            if date > task.end_time:
                raise TimeError("Notification date more than task end time")

        notification = Notification(user=user, message=message, date=date)
        task.notifications.append(notification)

        self._save(notification)

        return notification.id

    @log_decorator
    def set_missed_notifications_as_shown(self, user):
        """Marks missed notifications as shown"""
        missed_notifications = (self._session.query(Notification)
                                .filter_by(user=user)
                                .filter_by(shown=False)
                                .filter(Notification.date < datetime.now())
                                .all())

        for notification in missed_notifications:
            notification.shown = True

        self._session.commit()

        return missed_notifications

    @log_decorator
    def delete_notification(self, user, notification_id):
        """Deletes notification by id"""
        notification = self.get_notification(user, notification_id)

        self._delete(notification)

    @log_decorator
    def get_tag(self, user, tag_id):
        """Returns tag by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(Tag)
                    .filter_by(user=user)
                    .filter_by(id=tag_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Tag not found")

    @log_decorator
    def get_tags(self, user, task_id=None, name=None):
        """Returns list of tags that match specified filters"""
        return (self._session.query(Tag)
                .filter(Tag.user == user)
                .filter(or_(name is None, Tag.name == name))
                .join(Tag.tasks)
                .filter(or_(task_id is None, Task.id == task_id))
                .all())

    @log_decorator
    def edit_tag(self, user, tag_id, name=None):
        """Edits certain fields of tag"""
        tag = self.get_tag(user, tag_id)

        if name is not None:
            if self.get_tags(user, name=name) and name != tag.name:
                raise ValueError("Tag with specified name already exists")
            tag.name = name

        self._session.commit()

    @log_decorator
    def add_tag(self, user, name, task_id):
        """Сreates or add tag from appropriate data set

        :param user: tag owner
        :param name: tag name
        :param task_id: id of the task to which the tag is added
        :return: new notification id
        """
        task = self.get_task(user, task_id)

        tag = (self._session.query(Tag)
               .filter_by(user=user)
               .filter_by(name=name)
               .first())

        if tag in task.tags:
            raise AttachError("Tag already attached")
        elif tag is None:
            tag = Tag(user=user, name=name)
            
        tag.tasks.append(task)
        self._save(tag)

        return tag.id

    @log_decorator
    def delete_tag(self, user, tag_id, task_id=None):
        """Deletes tag entirely or for specific task"""
        tag = self.get_tag(user, tag_id)

        if task_id is not None:
            task = self.get_task(user, task_id)
            task.tags.remove(tag)

            self._session.commit()
        else:
            self._delete(tag)

    @log_decorator
    def get_task_template(self, user, task_template_id):
        """Returns task template by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(TaskTemplate)
                    .filter_by(user=user)
                    .filter_by(id=task_template_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("TaskTemplate not found")

    def _create_task_template(self, user, task):
        """Returns the template created from the task"""
        task_template = TaskTemplate(
            user=task.user, 
            name=task.name, 
            start_time=task.start_time, 
            end_time=task.end_time, 
            priority=task.priority, 
            description=task.description,
            folder_id=task.folder_id
        )

        self._save(task_template)

        return task_template

    @log_decorator
    def get_executor(self, user, executor_id):
        """Returns executor by id or raises exception if it doesn't exist"""
        try:
            return (self._session.query(Executor)
                    .filter_by(id=executor_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Executor not found")

    @log_decorator
    def add_executor(self, 
                     user, 
                     executor_user, 
                     task_id, 
                     in_subtasks=False):
        """Сreates or add executor from appropriate data set

        :param user: name of user to whose task executor is added
        :param executor_user: executor name
        :param task_id: id of the task to which the executor is added
        :param in_subtasks: flag indicates addition of executor in subtasks
        :return: new executor id
        """
        if user == executor_user:
            raise AttachError("User and executor are the same")

        task = self.get_task(user, task_id)
        executor = (self._session.query(Executor)
                     .filter_by(user=executor_user)
                     .first())

        if executor is not None:
            if executor in task.executors:
                raise AttachError("User is already a task executor")
        else:
            executor = Executor(user=executor_user)

        task.executors.append(executor)
        if in_subtasks:
            self._add_subtasks_executor(task, executor)

        self._save(executor)

        return executor.id

    def _add_subtasks_executor(self, task, executor):
        """Adds executor to subtasks"""
        for subtask in task.subtasks:
            if executor not in subtask.executors:
                subtask.executors.append(executor)
            self._add_subtasks_executor(subtask, executor)

    @log_decorator
    def delete_executor(self, 
                         user, 
                         executor_user, 
                         task_id, 
                         in_subtasks=False):
        """Deletes executor from appropriate data set

        :param user: name of user to whose task executor is deleted
        :param executor_user: executor name
        :param task_id: id of the task to which the executor is deleted
        :param in_subtasks: flag indicates deletion of executor in subtasks
        """
        if user == executor_user:
            raise DetachError("User and executor are the same")

        task = self.get_task(user, task_id)
        executor = (self._session.query(Executor)
                     .filter_by(user=executor_user)
                     .first())

        if executor is None:
            raise DetachError("User isn't a executor")
        elif executor not in task.executors:
            raise DetachError("User isn't a task executor")
        else:
            task.executors.remove(executor)
            if in_subtasks:
                self._delete_subtasks_executor(task, executor)
            if len(executor.tasks) == 0:
                self._delete(executor)

        self._session.commit()

    def _delete_subtasks_executor(self, task, executor):
        """Deletes executor from subtasks"""
        for subtask in task.subtasks:
            if executor in subtask.executors:
                subtask.executors.remove(executor)
            self._delete_subtasks_executor(subtask, executor)

    @log_decorator
    def get_executors(self, user, task_id=None, executor_user=None):
        """Returns list of executors that match specified filters"""
        return (self._session.query(Executor)
                .filter(
                    or_(
                        executor_user is None, 
                        Executor.user == executor_user
                    )
                )
                .join(Executor.tasks)
                .filter(Task.user == user)
                .filter(or_(task_id is None, Task.id == task_id))
                .all())

    def _delete(self, obj):
        """Deletes object from database"""
        self._session.delete(obj)
        self._session.commit()

    def _save(self, obj):
        """Saves object to database"""
        self._session.add(obj)
        self._session.commit()