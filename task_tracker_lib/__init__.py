"""
    This package contains all functionality to work with tracker library

    Packages:
        api - contains api for interacting with database models
        models - contains database models used in library
        tests - contains tests that check library api 

    Modules:
        database.py - describes type for creating database models and
                      function for connecting to database

    Usage:
        1) import library into your project
        2) create object of library interface using database_url
           (url format: "dialect+driver://username:password@host:port/database")
        3) use interface methods for your needs

    Example:
        >>> from task_tracker_lib.api.interface import Interface
        >>> database_url = "sqlite:///example.db"
        >>> library_interface = Interface(database_url)
        >>> user = "example_user"

        >>> task_id = library_interface.create_task(user=user, name="task")
        >>> subtask_id = library_interface.create_task(user=user, name="subtask")

        >>> library_interface.attach_subtask(user, task_id, subtask_id)
        >>> task = library_interface.get_task(user, task_id)
        >>> print(task)
        Task id=1 name=task:
         start time - 01 September 2018  00:05
         end time - no end time
         subtasks ids - [2]
         description - no description
         status - IN_PROGRESS
         priority - LOW
         folder - no folder
         tags - [no tags]
         executors - [no executors]
        >>> print(subtask_id)
        2
        
        >>> library_interface.get_task_tree(user, task_id)
        [<Task(id=1 name=task)>, [<Task(id=2 name=subtask)>]]
        
        >>> library_interface.complete_task(user, task_id)
        >>> print(task.status)
        DONE
        >>> print(subtasks.status)
        DONE
        
        >>> library_interface.add_tag(user=user, name="tag", task_id=task_id)
        >>> library_interface.get_tags(user=user)
        [<Tag(id=1, name=tag 1)>, <Tag(id=2, name=tag 2)>]

    Features:
        This library allows you to create tasks for further execution,
        create planners that automatically create tasks at specified 
        time interval, group tasks using tags and folders, сreate notifications 
        for tasks, give the right to execute tasks to other users
"""