"""
    This model contains type for creating database models and
    function for connecting to database
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import (
    sessionmaker,
    scoped_session
)

Base = declarative_base()


def get_session(database_url):
    engine = create_engine(
        database_url, 
        connect_args={'check_same_thread': False}
    )
    Session = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    return scoped_session(Session)