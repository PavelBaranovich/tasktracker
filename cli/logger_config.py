import os
import logging

DEFAULT_LOGGER_CONFIG = {
    "message_format": "[%(asctime)s]%(name)s, %(levelname)s : %(message)s",
    "date_format" : "%d.%m.%Y %H:%M:%S",
    "filename": "logger.log",
    "directory": "~/.python_tracker/",
    "level": logging.DEBUG,
    "enabled": True
}


def init_logger(logger, logger_config):
    enabled = DEFAULT_LOGGER_CONFIG["enabled"] or logger_config["enabled"]
    if enabled:
        directory = os.path.expanduser(DEFAULT_LOGGER_CONFIG["directory"] or 
                                       logger_config["directory"])
        if not os.path.isdir(directory):
            os.mkdir(directory)
        filename = (DEFAULT_LOGGER_CONFIG["filename"] or
                    logger_config["filename"])
        message_format = (DEFAULT_LOGGER_CONFIG["message_format"] or
                          logger_config["message_format"])
        date_format = (DEFAULT_LOGGER_CONFIG["date_format"] or
                       logger_config["date_format"])
        level = (DEFAULT_LOGGER_CONFIG["level"] or
                 logger_config["level"])

        file_path = os.path.join(directory, filename)
        file_handler = logging.FileHandler(file_path)
        formatter = logging.Formatter(message_format, date_format)
        file_handler.setFormatter(formatter)

        logger.setLevel(level)
        logger.addHandler(file_handler)
        logger.disabled = False
    else:
        logger.disabled = True