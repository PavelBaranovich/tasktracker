import sys
import argparse
from datetime import datetime

import cli.handlers as handlers
from task_tracker_lib.models.planner import IntervalType
from task_tracker_lib.models.task import (
    Priority,
    Status
)

DATE_FORMAT = '%d.%m.%Y %H:%M'
ENTERED_DATE_EXAMPLE = datetime.now().strftime(DATE_FORMAT)

TASK_SUBPARSER_NAME = "task"
PLANNER_SUBPARSER_NAME = "planner"
TAG_SUBPARSER_NAME = "tag"
FOLDER_SUBPARSER_NAME = "folder"
EXECUTOR_SUBPARSER_NAME = "executor"
NOTIFICATION_SUBPARSER_NAME = "notification"
CONFIG_SUBPARSER_NAME = "config"

TRACKER_PARSER_DESTINATION = "object"
OBJECT_SUBPARSER_DESTINATION = "action"

SHOW_ACTION_SUBPARSER_NAME = "show"
ADD_ACTION_SUBPARSER_NAME = "add"
COMPLETE_ACTION_SUBPARSER_NAME = "complete"
ATTACH_ACTION_SUBPARSER_NAME = "attach"
DETACH_ACTION_SUBPARSER_NAME = "detach"
DELETE_ACTION_SUBPARSER_NAME = "delete"
EDIT_ACTION_SUBPARSER_NAME = "edit"
LIST_ACTION_SUBPARSER_NAME = "list"
TREE_ACTION_SUBPARSER_NAME = "tree"
MOVE_ACTION_SUBPARSER_NAME = "move"
REPLACE_ACTION_SUBPARSER_NAME = "replace"


def convert_str_to_datetime(text):
    try:
        return datetime.strptime(text, DATE_FORMAT)
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid date format")


def convert_str_to_positive_int(text):
    if text.isdigit() and int(text) != 0:
        return int(text)
    else:
        raise argparse.ArgumentTypeError("Positive number expected")


def convert_str_to_bool(text):
    if text.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif text.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected')


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        print("error: {}".format(message), file=sys.stderr)
        self.print_help()
        sys.exit(2)


def parse_args():
    parser = DefaultHelpParser(
        prog="tracker",
        description="Simple task tracker"
    )

    subparsers = parser.add_subparsers(dest=TRACKER_PARSER_DESTINATION)
    subparsers.required = True

    task_subparser = subparsers.add_parser(
        TASK_SUBPARSER_NAME,
        help="Main application object. Created for further execution "
             "by author or third-party users (executors)"
    )
    init_task_subparser(task_subparser)

    planner_subparser = subparsers.add_parser(
        PLANNER_SUBPARSER_NAME,
        help="Object for automatic creation tasks"
    )
    init_planner_subparser(planner_subparser)

    tag_subparser = subparsers.add_parser(
        TAG_SUBPARSER_NAME,
        help="Object for categorization and description of tasks"
    )
    init_tag_subparser(tag_subparser)

    folder_subparser = subparsers.add_parser(
        FOLDER_SUBPARSER_NAME,
        help="Object for grouping tasks"
    )
    init_folder_subparser(folder_subparser)

    executor_subparser = subparsers.add_parser(
        EXECUTOR_SUBPARSER_NAME,
        help="Users who execute other user's tasks"
    )
    init_executor_subparser(executor_subparser)

    notification_subparser = subparsers.add_parser(
        NOTIFICATION_SUBPARSER_NAME,
        help="Object to remind of a pending task"
    )
    init_notification_subparser(notification_subparser)

    config_subparser = subparsers.add_parser(
        CONFIG_SUBPARSER_NAME,
        help="Object for configuring logger, database and user"
    )
    init_config_subparser(config_subparser)
 
    return parser.parse_args()


def init_task_subparser(task_subparser):
    task_subparsers = task_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with task"
    )
    task_subparsers.required = True

    task_show = task_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows task details"
    )
    task_show.set_defaults(func=handlers.handle_task_show)
    task_show.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )

    task_list = task_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows tasks with specified parameters (filters)"
    )
    task_list.set_defaults(func=handlers.handle_task_list)
    task_list.add_argument(
        "-n", "--name", 
        type=str, 
        help="Tasks with specific name"
    )
    task_list.add_argument(
        "-S", "--status",
        type=Status.get_from_string,
        choices=list(Status),
        help="Tasks with specific status"
    )
    task_list.add_argument(
        "-t", "--tree_id",
        type=int,
        help="Tasks with specific tree id"
    )
    task_list.add_argument(
        "-s", "--start_time",
        type=convert_str_to_datetime,
        help="Tasks with specific start time. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_list.add_argument(
        "-e", "--end_time",
        type=convert_str_to_datetime,
        help="Tasks with specific end time. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_list.add_argument(
        "-p", "--priority",
        type=Priority.get_from_string,
        choices=list(Priority),
        help="Tasks with specific priority"
    )
    task_list.add_argument(
        "-d", "--description",
        type=str,
        help="Tasks with specific description"
    )
    task_list.add_argument(
        "-P", "--parent_id",
        type=int,
        dest="parent_task_id",
        help="Tasks with specific parent task (parent task id)"
    )
    task_list.add_argument(
        "-l", "--planner_id",
        type=int,
        help="Tasks with specific planner (planner id)"
    )
    task_list.add_argument(
        "-f", "--folder_id",
        type=int,
        help="Tasks with specific folder (folder id)"
    )
    task_list.add_argument(
        "-a", "--executor",
        action="store_true",
        dest="as_executor",
        help="Tasks in which user is executor"
    )

    task_add = task_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Сreates new task"
    )
    task_add.set_defaults(func=handlers.handle_task_add)
    task_add.add_argument(
        "name", 
        type=str, 
        help="Task name"
    )
    task_add.add_argument(
        "-s", "--start_time",
        type=convert_str_to_datetime,
        help="Task start time. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_add.add_argument(
        "-e", "--end_time",
        type=convert_str_to_datetime,
        help="Task end time (time after which the task will fail). "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_add.add_argument(
        "-p", "--priority",
        type=Priority.get_from_string,
        choices=list(Priority),
        help="Task priority"
    )
    task_add.add_argument(
        "-d", "--description",
        type=str,
        help="Task description"
    )

    task_tree = task_subparsers.add_parser(
        TREE_ACTION_SUBPARSER_NAME,
        help="Shows task tree"
    )
    task_tree.set_defaults(func=handlers.handle_task_tree)
    task_tree.add_argument(
        "task_id",
        type=int,
        help="Root task id"
    )
    task_tree.add_argument(
        "-s", "--space_count",
        type=int,
        dest="indent",
        default=4,
        help="Count of spaces per indent level"
    )
    task_tree.add_argument(
        "-d", "--depth",
        dest="max_depth",
        type=convert_str_to_positive_int,
        help="Depth (level) limit. Is positive number"
    )

    task_complete = task_subparsers.add_parser(
        COMPLETE_ACTION_SUBPARSER_NAME,
        help="Completes task and all subtasks"
    )
    task_complete.set_defaults(func=handlers.handle_task_complete)
    task_complete.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )

    task_attach = task_subparsers.add_parser(
        ATTACH_ACTION_SUBPARSER_NAME,
        help="Attaches task as subtask of another"
    )
    task_attach.set_defaults(func=handlers.handle_task_attach)
    task_attach.add_argument(
        "task_id",
        type=int,
        help="Parent task id"
    )
    task_attach.add_argument(
        "subtask_id",
        type=int,
        help="Subtask id"
    )
    task_attach.add_argument(
        "-p", "--executors",
        action="store_true",
        dest="with_executors",
        help="Inherit task executors into subtask"
    )

    task_detach = task_subparsers.add_parser(
        DETACH_ACTION_SUBPARSER_NAME,
        help="Detaches task as subtask of another"
    )
    task_detach.set_defaults(func=handlers.handle_task_detach)
    task_detach.add_argument(
        "subtask_id",
        type=int,
        help="Subtask id"
    )

    task_delete = task_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes task"
    )
    task_delete.set_defaults(func=handlers.handle_task_delete)
    task_delete.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )
    task_delete.add_argument(
        "-t", "--tree",
        action="store_true",
        dest="with_task_tree",
        help="Delete task tree (task and all subtasks)"
    )

    task_edit = task_subparsers.add_parser(
        EDIT_ACTION_SUBPARSER_NAME,
        help="Edits task details"
    )
    task_edit.set_defaults(func=handlers.handle_task_edit)
    task_edit.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )
    task_edit.add_argument(
        "-n", "--name", 
        type=str, 
        help="New task name"
    )
    task_edit.add_argument(
        "-s", "--start_time",
        type=convert_str_to_datetime,
        help="New task start time."
             "Default: planner creation time. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_edit.add_argument(
        "-e", "--end_time",
        type=convert_str_to_datetime,
        help="New task end time (time after which the task will fail). "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    task_edit.add_argument(
        "-p", "--priority",
        type=Priority.get_from_string,
        choices=list(Priority),
        help="New task priority"
    )
    task_edit.add_argument(
        "-d", "--description",
        type=str,
        help="New task description"
    )

    task_move = task_subparsers.add_parser(
        MOVE_ACTION_SUBPARSER_NAME,
        help="Moves task from application root or from folder "
             "to another folder. And conversely"
    )
    task_move.set_defaults(func=handlers.handle_task_move)
    task_move.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )
    task_move.add_argument(
        "-f", "--folder_id",
        type=int,
        help="New task folder id"
    )


def init_planner_subparser(planner_subparser):
    planner_subparsers = planner_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with planner"
    )
    planner_subparsers.required = True

    planner_show = planner_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows planner details"
    )
    planner_show.set_defaults(func=handlers.handle_planner_show)
    planner_show.add_argument(
        "planner_id",
        type=int,
        help="Planner id"
    )

    planner_list = planner_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows planners with specified parameters (filters)"
    )
    planner_list.set_defaults(func=handlers.handle_planner_list)
    planner_list.add_argument(
        "-I", "--interval_type",
        type=IntervalType.get_from_string,
        choices=list(IntervalType),
        help="Planners with specific interval type"
    )
    planner_list.add_argument(
        "-i", "--interval_count",
        type=int,
        help="Planners with specific intervals count"
    )
    planner_list.add_argument(
        "-l", "--last_time",
        dest="last_created_time",
        type=convert_str_to_datetime,
        help="Planners with specific date of the last task creation. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    planner_list.add_argument(
        "-t", "--total_count",
        dest="total_tasks_count",
        type=int,
        help="Planners with specific total tasks count"
    )
    planner_list.add_argument(
        "-c", "--created_count",
        dest="created_tasks_count",
        type=int,
        help="Planners with specific count of created tasks"
    )

    planner_add = planner_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Сreates new planner"
    )
    planner_add.set_defaults(func=handlers.handle_planner_add)
    planner_add.add_argument(
        "task_id",
        type=int,
        help="Id of template task for planner"
    )
    planner_add.add_argument(
        "interval_type",
        type=IntervalType.get_from_string,
        choices=list(IntervalType),
        help="Planner interval type (create every <interval_type>)"
    )
    planner_add.add_argument(
        "-i", "--interval_count",
        type=int,
        default=1,
        help="Count of planner interval type "
             "(create every <interval_count> <interval_type>)"
    )
    planner_add.add_argument(
        "-s", "--start_time",
        type=convert_str_to_datetime,
        help="Time after which task creation begins. "
             "Default: planner creation time. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    planner_add.add_argument(
        "-e", "--end_time",
        type=convert_str_to_datetime,
        help="Time after which task creation ends. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    planner_add.add_argument(
        "-t", "--total_count",
        dest="total_tasks_count",
        type=int,
        help="Total count of tasks to create"
    )

    planner_edit = planner_subparsers.add_parser(
        EDIT_ACTION_SUBPARSER_NAME,
        help="Edit planner details"
    )
    planner_edit.set_defaults(func=handlers.handle_planner_edit)
    planner_edit.add_argument(
        "planner_id",
        type=int,
        help="Planner id"
    )
    planner_edit.add_argument(
        "-T", "--task_id",
        type=int,
        help="New id of template task for planner"
    )
    planner_edit.add_argument(
        "-I", "--interval_type",
        type=IntervalType.get_from_string,
        choices=list(IntervalType),
        help="New planner interval type (create every <interval_type>)"
    )
    planner_edit.add_argument(
        "-i", "--interval_count",
        type=int,
        help="New count of planner interval type "
             "(create every <interval_count> <interval_type>)"
    )
    planner_edit.add_argument(
        "-s", "--start_time",
        type=convert_str_to_datetime,
        help="New time after which task creation begins. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    planner_edit.add_argument(
        "-e", "--end_time",
        type=convert_str_to_datetime,
        help="New time after which task creation ends. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    planner_edit.add_argument(
        "-t", "--total_count",
        dest="total_tasks_count",
        type=int,
        help="New total count of tasks to create"
    )

    planner_delete = planner_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes planner"
    )
    planner_delete.set_defaults(func=handlers.handle_planner_delete)
    planner_delete.add_argument(
        "planner_id",
        type=int,
        help="Planner id"
    )


def init_tag_subparser(tag_subparser):
    tag_subparsers = tag_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with task tag"
    )
    tag_subparsers.required = True

    tag_show = tag_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows tag details"
    )
    tag_show.set_defaults(func=handlers.handle_tag_show)
    tag_show.add_argument(
        "tag_id",
        type=int,
        help="Tag id"
    )

    tag_list = tag_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows tags with specified parameters (filters)"
    )
    tag_list.set_defaults(func=handlers.handle_tag_list)
    tag_list.add_argument(
        "-t", "--task_id",
        type=int,
        help="Tags for specific task"
    )
    tag_list.add_argument(
        "-n", "--name", 
        type=str,
        help="Tags with specific name"
    )

    tag_add = tag_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Сreates new tag"
    )
    tag_add.set_defaults(func=handlers.handle_tag_add)
    tag_add.add_argument(
        "name", 
        type=str, 
        help="Tag name"
    )
    tag_add.add_argument(
        "task_id",
        type=int,
        help="Id of task for tag add"
    )

    tag_edit = tag_subparsers.add_parser(
        EDIT_ACTION_SUBPARSER_NAME,
        help="Edits tag details"
    )
    tag_edit.set_defaults(func=handlers.handle_tag_edit)
    tag_edit.add_argument(
        "tag_id",
        type=int,
        help="Tag id"
    )
    tag_edit.add_argument(
        "-n", "--name", 
        type=str, 
        help="New tag name"
    )

    tag_delete = tag_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes tag"
    )
    tag_delete.set_defaults(func=handlers.handle_tag_delete)
    tag_delete.add_argument(
        "tag_id",
        type=int,
        help="Tag id"
    )
    tag_delete.add_argument(
        "-t","--task_id",
        type=int,
        help="Id of task whose tag is deleted"
    )


def init_folder_subparser(folder_subparser):
    folder_subparsers = folder_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with folder"
    )
    folder_subparsers.required = True

    folder_show = folder_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows folder details"
    )
    folder_show.set_defaults(func=handlers.handle_folder_show)
    folder_show.add_argument(
        "folder_id",
        type=int,
        help="Folder id"
    )
    folder_show.add_argument(
        "-t", "--tasks",
        action="store_true",
        help="Show folder tasks"
    )

    folder_list = folder_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows folders with specified parameters (filters)"
    )
    folder_list.set_defaults(func=handlers.handle_folder_list)
    folder_list.add_argument(
        "-n", "--name", 
        type=str, 
        help="Folder with specific name"
    )
    folder_list.add_argument(
        "-p", "--parent_id",
        dest="parent_folder_id",
        type=int,
        help="Folders with specific parent folder (parent folder id)"
    )
    folder_list.add_argument(
        "-m", "--message",
        type=int,
        help="Folders with specific message"
    )

    folder_tree = folder_subparsers.add_parser(
        TREE_ACTION_SUBPARSER_NAME,
        help="Shows folder tree"
    )
    folder_tree.set_defaults(func=handlers.handle_folder_tree)
    folder_tree.add_argument(
        "folder_id",
        type=int,
        help="Root folder id"
    )
    folder_tree.add_argument(
        "-s", "--space_count",
        type=int,
        dest="indent",
        default=4,
        help="Count of spaces per indent level"
    )
    folder_tree.add_argument(
        "-d", "--depth",
        dest="max_depth",
        type=convert_str_to_positive_int,
        help="Depth (level) limit. Is positive number"
    )

    folder_add = folder_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Сreates new folder"
    )
    folder_add.set_defaults(func=handlers.handle_folder_add)
    folder_add.add_argument(
        "name",
        type=str,
        help="Folder name"
    )
    folder_add.add_argument(
        "-p", "--parent_id",
        dest="parent_folder_id",
        type=int,
        help="Parent folder id"
    )

    folder_edit = folder_subparsers.add_parser(
        EDIT_ACTION_SUBPARSER_NAME,
        help="Edits folder details"
    )
    folder_edit.set_defaults(func=handlers.handle_folder_edit)
    folder_edit.add_argument(
        "folder_id",
        type=int,
        help="Folder id"
    )
    folder_edit.add_argument(
        "-n", "--name", 
        type=str, 
        help="New folder name"
    )

    folder_delete = folder_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes folder"
    )
    folder_delete.set_defaults(func=handlers.handle_folder_delete)
    folder_delete.add_argument(
        "folder_id",
        type=int,
        help="Folder id"
    )
    folder_delete.add_argument(
        "-t", "--tree",
        action="store_true",
        dest="with_subfolders_tree",
        help="Delete folder tree (folder and all subfolders)"
    )


def init_executor_subparser(executor_subparser):
    executor_subparsers = executor_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with task executors"
    )
    executor_subparsers.required = True

    executor_show = executor_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows executor details"
    )
    executor_show.set_defaults(func=handlers.handle_executor_show)
    executor_show.add_argument(
        "executor_id",
        type=int,
        help="Executor id"
    )

    executor_list = executor_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows executors for specific task"
    )
    executor_list.set_defaults(func=handlers.handle_executor_list)
    executor_list.add_argument(
        "-t", "--task_id",
        type=int,
        help="Executors for specific task"
    )
    executor_list.add_argument(
        "-n", "--name",
        type=str, 
        dest="executor_user",
        help="Executor username"
    )

    executor_add = executor_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Add new executor for task"
    )
    executor_add.set_defaults(func=handlers.handle_executor_add)
    executor_add.add_argument(
        "executor_user", 
        type=str, 
        metavar="name",
        help="Executor username"
    )
    executor_add.add_argument(
        "task_id",
        type=int,
        help="Id of task for executor add"
    )
    executor_add.add_argument(
        "-s", "--subtasks",
        action="store_false",
        dest="in_subtasks",
        help="Add executor to subtasks"
    )

    executor_delete = executor_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes tag"
    )
    executor_delete.set_defaults(func=handlers.handle_executor_delete)
    executor_delete.add_argument(
        "executor_user", 
        type=str, 
        metavar="name",
        help="Executor username"
    )
    executor_delete.add_argument(
        "task_id",
        type=int,
        help="Id of task for executor delete"
    )
    executor_delete.add_argument(
        "-s", "--subtasks",
        dest="in_subtasks",
        action="store_false",
        help="Delete executor from subtasks"
    )


def init_notification_subparser(notification_subparser):
    notification_subparsers = notification_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with task notification"
    )
    notification_subparsers.required = True

    notification_show = notification_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows notification details"
    )
    notification_show.set_defaults(func=handlers.handle_notification_show)
    notification_show.add_argument(
        "notification_id",
        type=int,
        help="Notification id"
    )

    notification_list = notification_subparsers.add_parser(
        LIST_ACTION_SUBPARSER_NAME,
        help="Shows notifications with specified parameters (filters)"
    )
    notification_list.set_defaults(func=handlers.handle_notification_list)
    notification_list.add_argument(
        "-d","--date",
        type=convert_str_to_datetime,
        help="Notifications with specific date. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    notification_list.add_argument(
        "-s", "--shown",
        type=convert_str_to_bool,
        help="Notifications that have been shown"
    )
    notification_list.add_argument(
        "-t", "--task_id",
        type=int,
        help="Notifications for specific task"
    )
    notification_list.add_argument(
        "-m", "--message",
        type=str, 
        help="Notification with specific massage"
    )

    notification_add = notification_subparsers.add_parser(
        ADD_ACTION_SUBPARSER_NAME,
        help="Сreates new task notification"
    )
    notification_add.set_defaults(func=handlers.handle_notification_add)
    notification_add.add_argument(
        "task_id",
        type=int,
        help="Id of task for notification"
    )
    notification_add.add_argument(
        "date",
        type=convert_str_to_datetime,
        help="Time when notification will be shown. "
             "Input example: '{}'".format(ENTERED_DATE_EXAMPLE)
    )
    notification_add.add_argument(
        "-m", "--message",
        type=str, 
        help="Notification massage"
    )

    notification_delete = notification_subparsers.add_parser(
        DELETE_ACTION_SUBPARSER_NAME,
        help="Deletes notification"
    )
    notification_delete.set_defaults(func=handlers.handle_notification_delete)
    notification_delete.add_argument(
        "notification_id",
        type=int,
        help="Notification id"
    )


def init_config_subparser(config_subparser):
    config_subparsers = config_subparser.add_subparsers(
        dest=OBJECT_SUBPARSER_DESTINATION,
        help="Actions with config"
    )
    config_subparsers.required = True

    config_show = config_subparsers.add_parser(
        SHOW_ACTION_SUBPARSER_NAME,
        help="Shows config details"
    )
    config_show.set_defaults(func=handlers.handle_config_show)

    config_replace = config_subparsers.add_parser(
        REPLACE_ACTION_SUBPARSER_NAME,
        help="Replaces the active config"
    )
    config_replace.set_defaults(func=handlers.handle_config_replace)
    config_replace.add_argument(
        "new_active_config_path",
        type=str,
        metavar="path",
        help="New config path"
    )

    config_edit = config_subparsers.add_parser(
        EDIT_ACTION_SUBPARSER_NAME,
        help="Edits config details"
    )
    config_edit.set_defaults(func=handlers.handle_config_edit)
    config_edit.add_argument(
        "section", 
        type=str, 
        help="Config section"
    )
    config_edit.add_argument(
        "setting",
        type=str, 
        help="Key of custom config field"
    )
    config_edit.add_argument(
        "value",
        type=str, 
        help="Value of custom config field"
    )