import os
import getpass
import configparser

from cli.logger_config import DEFAULT_LOGGER_CONFIG

DEFAULT_CONFIG_FILENAME = "config.ini"
DEFAULT_CONFIG_DIRECTORY =  os.path.join(
    os.environ['HOME'], 
    ".python_tracker/"
)
DEFAULT_DATABASE_URL = ''.join([
    "sqlite:///",
    DEFAULT_CONFIG_DIRECTORY,
    "tracker.db"
])
DEFAULT_USER = getpass.getuser()


class ConfigManager:
    def __init__(self):
        self._active_config = configparser.ConfigParser(interpolation=None)

        self.path = os.path.join(
            DEFAULT_CONFIG_DIRECTORY, 
            DEFAULT_CONFIG_FILENAME
        )

        if os.path.exists(self.path):
            self._active_config.read(self.path)
        else:
            self._create_default_config()
        
    def _create_default_config(self):
        self._active_config["logging"] = DEFAULT_LOGGER_CONFIG

        self._active_config["database"] = {
            "database_url": DEFAULT_DATABASE_URL
        }

        self._active_config["user"] = {"username": DEFAULT_USER}

        self._write_active_config()

    def _write_active_config(self):
        directory = os.path.dirname(self.path)
        if not os.path.isdir(directory):
            os.mkdir(directory)

        with open(self.path, 'w') as configfile:
           self._active_config.write(configfile)

    def replace_active_config(self, new_active_config_path):
        if os.path.exists(new_active_config_path):
            self._active_config.read(new_active_config_path)
            self._write_active_config()
        else:
            raise ValueError("New config not found")

    def set_active_config_settings(self, section, setting, value):
        self._active_config.set(section, setting, value)

        self._write_active_config()

    def get_active_config_settings(self, section):
        try:
            return dict(self._active_config.items(section))
        except configparser.Error:
            raise KeyError("Active config section not found")
