import textwrap


def handle_planners_tasks(user, library_interface):
    library_interface.generate_planners_tasks(user)


def handle_missed_notifications(user, library_interface):
    notifications = library_interface.set_missed_notifications_as_shown(user)
    if notifications:
        print("Missed notifications:")
        for notification in notifications:
            print(textwrap.indent(str(notification), " " * 4))
            print()


def handle_task_add(user, library_interface, args_dict):
    task_id = library_interface.create_task(user, **args_dict)
    print("Task with id {} added.".format(task_id))


def handle_task_show(user, library_interface, args_dict):
    task = library_interface.get_task(user, **args_dict)
    print(task)


def handle_task_list(user, library_interface, args_dict):
    tasks = library_interface.get_tasks(user, **args_dict)
    if tasks:
        for task in tasks:
            print(task)
            print()
    else:
        print("No corresponding tasks.")


def handle_task_tree(user, library_interface, args_dict):
    task_tree = library_interface.get_task_tree(
        user, 
        task_id=args_dict.pop("task_id")
    )
    _print_tree(task_tree, **args_dict)


def _print_tree(tree, indent=4, depth=1, max_depth=None):
    if (max_depth is not None) and (depth > max_depth):
        return

    for node in tree:
        if isinstance(node, list):
            _print_tree(node, indent, depth + 1, max_depth)
        else:
            print(textwrap.indent(str(node), " " * indent * (depth - 1)))
            print()


def handle_task_complete(user, library_interface, args_dict):
    library_interface.complete_task(user, **args_dict)
    print("Task is completed.")


def handle_task_attach(user, library_interface, args_dict):
    library_interface.attach_subtask(user, **args_dict)
    print("Task is attached as subtask.")


def handle_task_detach(user, library_interface, args_dict):
    library_interface.detach_subtask(user, **args_dict)
    print("Subtask is detached.")


def handle_task_delete(user, library_interface, args_dict):
    library_interface.delete_task(user, **args_dict)
    print("Task is deleted.")


def handle_task_edit(user, library_interface, args_dict):
    args_count = sum(value is not None for value in args_dict.values())
    if args_count == 1:
        print("Please, specify fields to edit.")
    else:
        library_interface.edit_task(user, **args_dict)
        print("Task is edited.")


def handle_task_move(user, library_interface, args_dict):
    if args_dict.get("folder_id") is not None:
        library_interface.add_task_to_folder(user, **args_dict)
        print("Task is moved to folder.")
    else:
        library_interface.delete_task_from_folder(user, **args_dict)
        print("Task is removed from folder.")


def handle_notification_add(user, library_interface, args_dict):
    notification_id = library_interface.add_notification(user, **args_dict)
    print("Notification with id {} added.".format(notification_id))


def handle_notification_show(user, library_interface, args_dict):
    notification = library_interface.get_notification(user, **args_dict)
    print(notification)


def handle_notification_list(user, library_interface, args_dict):
    notifications = library_interface.get_notifications(user, **args_dict)
    if notifications:
        for notification in notifications:
            print(notification)
            print()
    else:
        print("No corresponding notifications.")


def handle_notification_delete(user, library_interface, args_dict):
    library_interface.delete_notification(user, **args_dict)
    print("Task notification is deleted.")


def handle_folder_add(user, library_interface, args_dict):
    folder_id = library_interface.add_folder(user, **args_dict)
    print("Folder with id {} added.".format(folder_id))


def handle_folder_show(user, library_interface, args_dict):
    folder = library_interface.get_folder(user, args_dict.get("folder_id"))
    print(folder)
    print()

    if args_dict.get("tasks") is not None:
        for task in folder.tasks:
            print(textwrap.indent(str(task), " " * 4))
            print()


def handle_folder_edit(user, library_interface, args_dict):
    args_count = sum(value is not None for value in args_dict.values())
    if args_count == 1:
        print("Please, specify fields to edit.")
    else:
        library_interface.edit_folder(user, **args_dict)
        print("Folder is edited.")


def handle_folder_tree(user, library_interface, args_dict):
    folder_tree = library_interface.get_folder_tree(
        user, 
        folder_id=args_dict.pop("folder_id")
    )
    _print_tree(folder_tree, **args_dict)


def handle_folder_list(user, library_interface, args_dict):
    folders = library_interface.get_folders(user, **args_dict)
    if folders:
        for folder in folders:
            print(folder)
            print()
    else:
        print("No corresponding folders.")


def handle_folder_delete(user, library_interface, args_dict):
    library_interface.delete_folder(user, **args_dict)
    print("Folder is deleted.")


def handle_planner_add(user, library_interface, args_dict):
    planner_id = library_interface.create_planner(user, **args_dict)
    print("Planner with id {} added.".format(planner_id))


def handle_planner_list(user, library_interface, args_dict):
    planners = library_interface.get_planners(user, **args_dict)
    if planners:
        for planner in planners:
            print(planner)
            print()
    else:
        print("No corresponding planners.")


def handle_planner_edit(user, library_interface, args_dict):
    args_count = sum(value is not None for value in args_dict.values())
    if args_count == 1:
        print("Please, specify fields to edit.")
    else:
        library_interface.edit_planner(user, **args_dict)
        print("Planner is edited.")


def handle_planner_show(user, library_interface, args_dict):
    planner = library_interface.get_planner(user, **args_dict)
    print(planner)


def handle_planner_delete(user, library_interface, args_dict):
    library_interface.delete_planner(user, **args_dict)
    print("Planner is deleted.")


def handle_tag_add(user, library_interface, args_dict):
    tag_id = library_interface.add_tag(user, **args_dict)
    print("Tag with id {} added.".format(tag_id))


def handle_tag_show(user, library_interface, args_dict):
    tag = library_interface.get_tag(user, **args_dict)
    print(tag)


def handle_tag_list(user, library_interface, args_dict):
    tags = library_interface.get_tags(user, **args_dict)
    if tags:
        for tag in tags:
            print(tag)
            print()
    else:
        print("No corresponding tags.")


def handle_tag_edit(user, library_interface, args_dict):
    args_count = sum(value is not None for value in args_dict.values())
    if args_count == 1:
        print("Please, specify fields to edit.")
    else:
        library_interface.edit_tag(user, **args_dict)
        print("Tag is edited.")


def handle_tag_delete(user, library_interface, args_dict):
    library_interface.delete_tag(user, **args_dict)
    print("Task tag is deleted.")


def handle_executor_add(user, library_interface, args_dict):
    executor_id = library_interface.add_executor(user, **args_dict)
    print("Executor with id {} added.".format(executor_id))


def handle_executor_show(user, library_interface, args_dict):
    executor = library_interface.get_executor(user, **args_dict)
    print(executor)


def handle_executor_list(user, library_interface, args_dict):
    executors = library_interface.get_executors(user, **args_dict)
    if executors:
        for executor in executors:
            print(executor)
            print()
    else:
        print("No corresponding executors.")


def handle_executor_delete(user, library_interface, args_dict):
    library_interface.delete_executor(user, **args_dict)
    print("Task executor is deleted.")


def handle_config_show(user, config_manager, args_dict):
    with open(config_manager.path, 'r') as configfile:
        print(configfile.read())
    

def handle_config_edit(user, config_manager, args_dict):
    config_manager.set_active_config_settings(**args_dict)
    print("Config is edited.")


def handle_config_replace(user, config_manager, args_dict):
    config_manager.replace_active_config(**args_dict)
    print("Config is replaced.")