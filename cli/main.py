import sys
from copy import copy
from argparse import ArgumentTypeError
from functools import wraps

import cli.handlers as handlers
from cli.parser import (
    parse_args,
    TRACKER_PARSER_DESTINATION,
    OBJECT_SUBPARSER_DESTINATION,
    CONFIG_SUBPARSER_NAME
)
from cli.config_manager import ConfigManager
from cli.logger_config import init_logger
from task_tracker_lib.api.interface import Interface
from task_tracker_lib.api.logger import get_logger
from task_tracker_lib.api.exceptions import TrackerLibraryError


def handle_errors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except TrackerLibraryError as e:
            print("Error in library: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except KeyError as e:
            print("Invalid key: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except ValueError as e:
            print("Invalid value: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except ArgumentTypeError as e:
            print("Invalid console argument: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except Exception as e:
            print("Internal error: {}".format(e), file=sys.stderr)
            sys.exit(1)

    return wrapper


@handle_errors
def main():
    config_manager = ConfigManager()

    database_config = config_manager.get_active_config_settings("database")
    library_interface = Interface(database_config["database_url"])

    logger_config = config_manager.get_active_config_settings("logging")
    logger = get_logger()
    init_logger(logger, logger_config)

    user_config = config_manager.get_active_config_settings("user")
    user = user_config["username"]

    args = parse_args()

    handlers.handle_planners_tasks(user, library_interface)
    handlers.handle_missed_notifications(user, library_interface)

    args_dict = vars(copy(args))
    args_dict.pop("func")
    args_dict.pop(OBJECT_SUBPARSER_DESTINATION)
    args_dict.pop(TRACKER_PARSER_DESTINATION)

    if getattr(args, TRACKER_PARSER_DESTINATION) == CONFIG_SUBPARSER_NAME:
        args.func(user, config_manager, args_dict)
    else:
        args.func(user, library_interface, args_dict)


if __name__ == "__main__":
    main()