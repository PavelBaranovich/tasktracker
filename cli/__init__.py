"""
    This package contains all functionality to interact with 
    Python tracker library the command line

    Modules:
        config_manager.py - describes class used to create, replace and
                            interact with the config
        handlers.py - describes logic for handling actions on command line
        logger_config.py - describes initialization of logger
        main.py - describes steps for calling program from command line
                  (entry point when you run commands)
        parser.py - describes initialization of command-line parsers, 
                    string conversion functions, and wrapper class 
                    for main parser
"""